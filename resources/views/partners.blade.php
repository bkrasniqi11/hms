@extends('layouts.app');
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                data-target=".add-article-unit-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                        </button>

                        <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                        </button>
                        </p>
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <br>
                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-partners" class="table table-striped table-bordered focus-on">
                            <thead class="thead-default">
                            <tr>
                                <th>Partner</th>
                                <th>Partner kategoria</th>
                                <th>Nr.Fiskal</th>
                                <th>Nr.Biznesit</th>
                                <th>P.Tvsh</th>
                                <th>Adresa</th>
                                <th>Tel</th>
                                <th>Email</th>
                                <th>Statusi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($partners as $partner)
                                <tr id="{{$partner->id_partner}}" class="unfocused">
                                    <td>{{$partner->partner_name}}</td>
                                    <td>{{$partner->partnersCategory->partner_category_description}}</td>
                                    <td>{{$partner->partner_fiscal_number}}</td>
                                    <td>{{$partner->partner_bussines_number}}</td>
                                    <td>{{$partner->partner_tvsh}}%</td>
                                    <td>{{$partner->city->city_name}}, {{$partner->partner_address}}</td>
                                    <td>{{$partner->partner_tel}} </td>
                                    <td>{{$partner->partner_email}}</td>
                                    <td>Aktiv</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@stop


{{--ADD FORM--}}
<div class="modal fade add-article-unit-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto njësi për artikuj </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","action"=>"PartnersController@store","method"=>"POST", "onsubmit" => "return validateForm()"]) !!}


                <div class="form-group">
                    <label for="Pname">Emri<span class="text-danger">*</span></label>
                    <input type="text" name="partner_name" parsley-trigger="change" required="true"
                           placeholder="Emri" class="form-control" id="partner_name" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Kategoria">Kategoria</label>
                    <select class="form-control" id="partner_category" name="partner_category" required="true">
                        <option disabled selected>Zgjedh kategorin</option>
                        @foreach($partner_categorys as $partner_category)
                            <option value="{{$partner_category->id_partner_category}}">{{$partner_category->partner_category_description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="pfiscal">Nr.Fiskal<span class="text-danger">*</span></label>
                    <input type="text" name="partner_fiscal" parsley-trigger="change" required="true"
                           placeholder="Nr.Fiskal" class="form-control" id="partner_fiscal" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="pbusiness">Nr.Biznesit<span class="text-danger">*</span></label>
                    <input type="text" name="partner_business" parsley-trigger="change" required="true"
                           placeholder="Nr.Biznesit" class="form-control" id="partner_business" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="ptvsh">TVSH<span class="text-danger">*</span></label>
                    <input type="text" name="partner_tvsh" parsley-trigger="change" required="true"
                           placeholder="TVSH" class="form-control" id="partner_tvsh" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="city">Qyteti</label>
                    <select class="form-control" id="partner_city" name="partner_city" required="true">
                        <option disabled selected>Zgjedh qyteti</option>
                        @foreach($citys as $city)
                            <option value="{{$city->id_city}}">{{$city->city_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="padress">Adresa<span class="text-danger">*</span></label>
                    <input type="text" name="partner_adress" parsley-trigger="change" required="true"
                           placeholder="Adresa" class="form-control" id="partner_adress" data-parsley-id="4">
                </div>


                <div class="form-group">
                    <label for="ptel">Tel<span class="text-danger">*</span></label>
                    <input type="text" name="partner_tel" parsley-trigger="change" required="true"
                           placeholder="Tel" class="form-control" id="partner_tel" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="pemail">Email<span class="text-danger">*</span></label>
                    <input type="email" name="partner_email" parsley-trigger="change" required
                           placeholder="Email" class="form-control" id="partner_email" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="pweb">Uebsajti</label>
                    <input type="text" name="partner_web" parsley-trigger="change"
                           placeholder="Uebsajti" class="form-control" id="partner_web" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Description">Përshkrim</label>
                    <input type="text" name="partner_desc" parsley-trigger="change"
                           placeholder="Përshkrim" class="form-control" id="partner_desc" data-parsley-id="4">
                </div>


                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>
                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}

@section('add-script')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    {{--<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>--}}
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.alert').hide();

            }, 4000);
        });

        //Sweet Alerts warning message
        $('#sa-warning').click(function () {

            row = $("#table-partners").find('tbody tr.focused');
            if (row.size() < 1) {
                alert("Selekto partnerin");
                return;
            }
            var url = window.location.href;

            partner_id = row[0].id;
            partner_name = $("#" + partner_id).find("td");
            partner_name = $(partner_name[0]).text();

            swal({
                title: "Fshij partnerin: " + partner_name,
//                text: article_name,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary waves-effect',
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Fshij",
                cancelButtonText: "Jo",
                closeOnConfirm: false
            }, function () {

                $.ajax({

                    type: "delete",

                    url: url + "/" + partner_id,

                    data: {"partner": partner_id, "_token": "{{ csrf_token() }}"},

                    success: function (result) {
                        $("#" + partner_id).remove();
                        swal("Partneri u fshi me sukses", "", "success");

                    },
                    error: function (result) {
                        swal("Partneri ka dështuar të fshihet", "", "error");

                    }
                });


            });


        });

        $("#table-partners").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-partners").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });

    </script>

    <script>
        function validateForm() {
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var partner_name = $("#partner_name").val();
            var partner_category = $("#partner_category").val();
            var partner_fiscal = $("#partner_fiscal").val();
            var partner_business = $("#partner_business").val();
            var partner_tvsh = $("#partner_tvsh").val();
            var partner_city = $("#partner_city").val();
            var partner_adress = $("#partner_adress").val();
            var partner_tel = $("#partner_tel").val();
            var partner_email = $("#partner_email").val();
            var partner_web = $("#partner_web").val();
            var partner_desc = $("#partner_desc").val();

            if (partner_name == "" || partner_name.trim() == "" || partner_name.length < 2) {
                alert(txtAlert);
                $("#partner_name").focus();
                return false;
            }

            if (!isNaN(partner_name)) {
                alert(txtAlert);
                $("#partner_name").focus();
                return false;
            }

            if (partner_category == null) {
                alert(txtAlert);
                $("#partner_category").focus();
                return false;
            }

            if (partner_fiscal == "" || partner_fiscal.trim() == "" || isNaN(partner_fiscal)) {
                alert(txtAlert);
                $("#partner_fiscal").focus();
                return false;
            }

            if (partner_business == "" || partner_business.trim() == "" || isNaN(partner_business)) {
                alert(txtAlert);
                $("#partner_business").focus();
                return false;
            }

            if (partner_tvsh == "" || partner_tvsh.trim() == "" || partner_tvsh <1 || partner_tvsh >100) {
                alert(txtAlert);
                $("#partner_tvsh").focus();
                return false;
            }

            if (partner_city == null) {
                alert(txtAlert);
                $("#partner_city").focus();
                return false;
            }

            if (partner_adress == "" || partner_adress.trim() == "" || partner_adress.length < 2) {
                alert(txtAlert);
                $("#partner_adress").focus();
                return false;
            }

            if (!isNaN(partner_adress)) {
                alert(txtAlert);
                $("#partner_adress").focus();
                return false;
            }

            if (partner_tel == "" || partner_tel.trim() == "") {
                alert(txtAlert);
                $("#partner_tel").focus();
                return false;
            }

            if (partner_email == "" || partner_email.trim() == "" || partner_email.length < 2) {
                alert(txtAlert);
                $("#partner_email").focus();
                return false;
            }

            if (!isNaN(partner_email)) {
                    alert(txtAlert);
                    $("#partner_email").focus();
                    return false;
                }

            if (partner_web != "" || partner_web.trim() != "") {
                if (!isNaN(partner_web)) {
                    alert(txtAlert);
                    $("#partner_web").focus();
                    return false;
                }
            }
            if (partner_desc != "" || partner_desc.trim() != "") {
                if (!isNaN(partner_desc)) {
                    alert(txtAlert);
                    $("#partner_desc").focus();
                    return false;
                }
            }

            return true;
        }
    </script>
@stop