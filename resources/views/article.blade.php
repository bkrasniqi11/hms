@extends('layouts.app')
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                data-target=".add-article-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                        </button>

                        <button id="modal-edit-article" type="button" class="btn btn-default waves-effect waves-light"
                                data-toggle="modal"
                                data-target=".edit-article-modal">
                                <span class="btn-label"><i class="fa fa-pencil"></i>
                                </span>Edito
                        </button>


                        <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                        </button>
                        </p>
                    </div>
                </div>
                <br>

                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                    @endif
                @endforeach

                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-artc-unit"
                               class="table table-striped table-bordered focus-on table-row-hand">
                            <thead class="thead-default">
                            <tr>
                                <th>Artikulli</th>
                                <th>Nr.Serik</th>
                                <th>Njësia</th>
                                <th>Kategoria</th>
                                <th>TVSH</th>
                                <th>Çmimi Blerës</th>
                                <th>Çmimi Shitës</th>
                                <th>Sasia në stok</th>
                                <th>Sasia minimale</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                <tr id="{{ $article->id_article }}" class="unfocused">
                                    <td>{{ $article->article_name }}</td>
                                    <td>{{ $article->article_serial_number }}</td>
                                    <td>{{ $article->articleunit->article_unit_description }}</td>
                                    <td>{{ $article->articlecategory->article_category_description }}</td>
                                    <td>{{ $article->article_tvsh }}%</td>
                                    <td>{{ $article->article_bought_price }} &euro;</td>
                                    <td>{{ $article->article_selling_price }} &euro;</td>
                                    <td>{{ $article->article_quanity }}</td>
                                    <td>{{ $article->article_min_quantity }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop


{{--ADD FORM--}}
<div class="modal fade add-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto Artikull </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"", "method"=>"POST", "action"=>"ArticleController@store","onsubmit"=>"return validateForm()" ]) !!}

                <div class="form-group">
                    <label for="Artikulli">Artikulli<span class="text-danger">*</span></label>
                    <input type="text" name="article_name" parsley-trigger="change" required=""
                           placeholder="Artikulli" class="form-control" id="article_name" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="NrSerik">Numri serik i artikullit<span class="text-danger">*</span></label>
                    <input type="text" name="article_serial_number" parsley-trigger="change" required=""
                           placeholder="Nr.Serik" class="form-control" id="article_serial_number" data-parsley-id="6">
                </div>

                <div class="form-group">
                    <label for="Njesia">Njësia<span class="text-danger">*</span></label>
                    <select class="form-control" id="id_article_unit" name="id_article_unit">
                        <option disabled selected>Zgjedh Njësin</option>
                        @foreach($article_units as $article_unit)
                            <option value="{{$article_unit->id_article_unit}}">{{$article_unit->article_unit_description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="Kategoria">Kategoria<span class="text-danger">*</span></label>
                    <select class="form-control" id="id_article_category" name="id_article_category">
                        <option disabled selected>Zgjedh Kategorin</option>
                        @foreach($article_categories as $article_categorie)
                            <option value="{{$article_categorie->id_article_category}}">{{$article_categorie->article_category_description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="TVSH">TVSH</label>
                    <input type="text" name="article_tvsh" parsley-trigger="change" placeholder="TVSH"
                           required="" value="0.00" class="form-control" id="article_tvsh" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="CBleres">Çmimi blerës<span class="text-danger">*</span></label>
                    <input type="text" name="article_bought_price" parsley-trigger="change" required=""
                           placeholder="Çmimi blerës" class="form-control" id="article_bought_price"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="CShtites">Çmimi shitës<span class="text-danger">*</span></label>
                    <input type="text" name="article_selling_price" parsley-trigger="change" required=""
                           placeholder="Çmimi shitës" class="form-control" id="article_selling_price"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Sasia">Sasia<span class="text-danger">*</span></label>
                    <input type="text" name="article_quanity" parsley-trigger="change" required=""
                           placeholder="Sasia" class="form-control" id="article_quanity" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="min_sasia">Sasia minimale</label>
                    <input type="text" name="article_min_quantity" parsley-trigger="change" value="10" required=""
                           placeholder="Sasia minimale" class="form-control" id="article_min_quantity"
                           data-parsley-id="4">
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>
                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}

{{--EDIT FORM--}}
<div class="modal fade edit-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel"> Edito Artikullin </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","method"=>"PATCH","id"=>"form-modal-edit-article"]) !!}
                {{--,"action"=>"ArticleController@update"--}}

                <div class="form-group">
                    <label for="Artikulli">Artikulli</label>
                    <input type="text" name="article_name" parsley-trigger="change" required=""
                           placeholder="Artikulli" class="form-control" id="article_name" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="NrSerik">Numri serik i artikullit</label>
                    <input type="text" name="article_serial_number" parsley-trigger="change" required=""
                           placeholder="Nr.Serik" class="form-control" id="article_serial_number" data-parsley-id="6">
                </div>

                <div class="form-group">
                    <label for="Njesia">Njësia</label>
                    <select class="form-control" id="id_article_unit" name="id_article_unit">
                        <option disabled selected>Zgjedh Njësin</option>
                        @foreach($article_units as $article_unit)
                            <option value="{{$article_unit->id_article_unit}}">{{$article_unit->article_unit_description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="Kategoria">Kategoria</label>
                    <select class="form-control" id="id_article_category" name="id_article_category">
                        <option disabled selected>Zgjedh Kategorin</option>
                        @foreach($article_categories as $article_categorie)
                            <option value="{{$article_categorie->id_article_category}}">{{$article_categorie->article_category_description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="TVSH">TVSH</label>
                    <input type="text" name="article_tvsh" parsley-trigger="change" placeholder="TVSH"
                           required="" value="0.00" class="form-control" id="article_tvsh" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="CBleres">Çmimi blerës</label>
                    <input type="text" name="article_bought_price" parsley-trigger="change" required=""
                           placeholder="Çmimi blerës" class="form-control" id="article_bought_price"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="CShtites">Çmimi shitës</label>
                    <input type="text" name="article_selling_price" parsley-trigger="change" required=""
                           placeholder="Çmimi shitës" class="form-control" id="article_selling_price"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Sasia">Sasia</label>
                    <input type="text" name="article_quanity" parsley-trigger="change" required=""
                           placeholder="Sasia" class="form-control" id="article_quanity" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="min_sasia">Sasia minimale</label>
                    <input type="text" name="article_min_quantity" parsley-trigger="change" required=""
                           placeholder="Sasia minimale" class="form-control" id="article_min_quantity"
                           data-parsley-id="4">
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Submit
                    </button>

                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF EDIT FORM--}}

@section('add-script')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    {{--<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>--}}
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>


    <script>

        function validateForm() {
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var article_name = $('#article_name').val();
            var article_serial_number = $('#article_serial_number').val();
            var id_article_unit = $('#id_article_unit').val();
            var id_article_category = $('#id_article_category').val();
            var article_tvsh = $('#article_tvsh').val();
            var article_bought_price = $('#article_bought_price').val();
            var article_selling_price = $('#article_selling_price').val();
            var article_quanity = $('#article_quanity').val();
            var article_min_quantity = $('#article_min_quantity').val();

            //Check article_name
            if (article_name == "" || article_name.trim() == "" || article_name.length < 2) {
                alert(txtAlert);
                $("#article_name").focus();
                return false;
            }

            //Check article_serial_number
            if (article_serial_number == "" || article_serial_number.trim() == "" || article_serial_number.length < 2) {
                alert(txtAlert);
                $("#article_serial_number").focus();
                return false;
            }

            //Check id_article_unit
            if (id_article_unit == "" || id_article_unit <= 0) {
                alert(txtAlert);
                $("#id_article_unit").focus();
                return false;
            }

            //Check id_article_category
            if (id_article_category == "" || id_article_category <= 0) {
                alert(txtAlert);
                $("#id_article_category").focus();
                return false;
            }

            //Check article_tvsh
            if (article_tvsh == "" || article_tvsh.trim() == "" || article_tvsh.length < 2) {
                alert(txtAlert);
                $("#article_tvsh").focus();
                return false;
            }
            var numberArticleTvsh =  Number(article_tvsh);
            if(numberArticleTvsh<0 || numberArticleTvsh > 1){
                alert(txtAlert);
                $("#article_tvsh").focus();
                return false;
            }

            //Check article_bought_price
            if (article_bought_price == "" || article_bought_price.trim() == "" ) {
                alert(txtAlert);
                $("#article_bought_price").focus();
                return false;
            }
            var numberarticle_bought_price =  Number(article_bought_price);
            if(numberarticle_bought_price<0){
                alert(txtAlert);
                $("#article_bought_price").focus();
                return false;
            }

            //Check article_selling_price
            if (article_selling_price == "" || article_selling_price.trim() == "") {
                alert(txtAlert);
                $("#article_selling_price").focus();
                return false;
            }
            var numberarticle_selling_price =  Number(article_selling_price);
            if(numberarticle_selling_price<0){

                alert(txtAlert);
                $("#article_selling_price").focus();
                return false;
            }

            //Check article_quanity
            if (article_quanity == "" || article_quanity.trim() == "" ) {
                alert(txtAlert);
                $("#article_quanity").focus();
                return false;
            }
            var numberarticle_quanity =  Number(article_quanity);
            if(numberarticle_quanity<0){

                alert(txtAlert);
                $("#article_quanity").focus();
                return false;
            }

            //Check article_min_quantity
            if (article_min_quantity == "" || article_min_quantity.trim() == "") {
                alert(txtAlert);
                $("#article_min_quantity").focus();
                return false;
            }
            var numberarticle_min_quantity =  Number(article_min_quantity);
            if(numberarticle_min_quantity<0){
                alert(txtAlert);
                $("#article_min_quantity").focus();
                return false;
            }

            //Check quantity > mxin_quantity
            if(numberarticle_min_quantity > numberarticle_quanity){
                alert("Sasia minimale nuk mund të jetë më e madhe se sasia e artikullit.");
                $("#article_min_quantity").focus();
                return false;
            }




        }


    </script>


    <script>

        $("#modal-edit-article").click(function (ev) {

            row = $("#table-artc-unit").find('tbody tr.focused');
            if (row.size() < 1) {
                ev.stopPropagation();

                alert("Selekto artikullin");
                return;
            }

            var url = window.location.href;

            article_id = row[0].id;

            $.ajax({

                type: "GET",

                url: url + "/" + article_id,

                data: {"articles": article_id, "_token": "{{ csrf_token() }}"},

                success: function (result) {
                    $("#form-modal-edit-article").attr('action', url + "/" + article_id);
                    $("#form-modal-edit-article #article_name").val(result.article_name);
                    $("#form-modal-edit-article #article_serial_number").val(result.article_serial_number);
                    $("#form-modal-edit-article #id_article_unit").val(result.id_article_unit);
                    $("#form-modal-edit-article #id_article_category").val(result.id_article_category);
                    $("#form-modal-edit-article #article_serial_number").val(result.article_serial_number);
                    $("#form-modal-edit-article #article_tvsh").val(result.article_tvsh);
                    $("#form-modal-edit-article #article_bought_price").val(result.article_bought_price);
                    $("#form-modal-edit-article #article_selling_price").val(result.article_selling_price);
                    $("#form-modal-edit-article #article_quanity").val(result.article_quanity);
                    $("#form-modal-edit-article #article_min_quantity").val(result.article_min_quantity);


                },
                error: function (result) {
                    swal("Provoni përsëri", "", "error");
                }
            })
        });

    </script>


    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.alert').hide();

            }, 4000);

            //            $('form').parsley();
        });

        //Sweet Alerts warning message
        $('#sa-warning').click(function () {

            row = $("#table-artc-unit").find('tbody tr.focused');
            if (row.size() < 1) {
                alert("Selekto artikullin");
                return;
            }
            var url = window.location.href;

            article_id = row[0].id;
            article_name = $("#" + article_id).find("td");
            article_name = $(article_name[0]).text();

            swal({
                title: "Fshij artikullin: " + article_name,
//                text: article_name,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary waves-effect',
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Fshij",
                cancelButtonText: "Jo",
                closeOnConfirm: false
            }, function () {

                $.ajax({

                    type: "delete",

                    url: url + "/" + article_id,

                    data: {"articles": article_id, "_token": "{{ csrf_token() }}"},

                    success: function (result) {
                        $("#" + article_id).remove();
                        swal("Artikulli u fshi me sukses", "", "success");

                    },
                    error: function (result) {
                        swal("Artikulli ka dështuar të fshihet", "", "error");

                    }
                });


            });


        });


        $("#table-artc-unit").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-artc-unit").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });

    </script>
@stop