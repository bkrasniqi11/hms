<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HM-SYSTEM BY BK">
    <meta name="author" content="Bardh Krasniqi">

    <!-- App Favicon -->
    {{--<link rel="shortcut icon" href="images/favicon.ico">--}}

    <!-- App title -->
    <title>HM-System</title>


    <!-- App CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" media="print" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"  />

    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>

    @yield("head-scripts")

</head>

<body class="fixed-left">
<header id="topnav">

    @include('includes.topbar')


    <!-- end topbar-main -->
   @include('includes.navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="wrapper">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">

                <h4 class="page-title">@yield('page-title')</h4>
            </div>
        </div>


        @yield("body")

    <!-- Footer -->
        <footer class="footer text-right">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        2018 - Bardh Krasniqi
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

    </div> <!-- container -->

</div> <!-- End wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

<!-- following js will activate the menu in left side bar based on url -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#navigation a").each(function () {
            if (this.href == window.location.href) {
                $(this).addClass("active");
                $(this).parent().addClass("active"); // add active to li of the current link
                $(this).parent().parent().prev().addClass("active"); // add active class to an anchor
                $(this).parent().parent().prev().click(); // click the item to make it drop
            }
        });
    });
</script>

@yield('add-script')


</body>
</html>
