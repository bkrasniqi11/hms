@extends('layouts.app');
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                data-target=".add-article-unit-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                        </button>

                        <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                        </button>
                    </p>
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))

                        <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                        @endif
                        @endforeach
                    </div>
                </div>

                <br>

                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-artc-unit" class="table table-striped table-bordered focus-on">
                            <thead class="thead-default">
                            <tr>
                                <th>Tavolinat</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tables as $table)
                                <tr id="{{$table->id_order_table}}">
                                    <td>{{$table->order_table_description}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop

{{--ADD FORM--}}
<div class="modal fade add-article-unit-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto tavolinë </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","action"=>"OrderTablesController@store","method"=>"POST", "onsubmit" =>"return validateForm()"]) !!}


                <div class="form-group">
                    <label for="TableName">Emri i tavolinës<span class="text-danger">*</span></label>
                    <input type="text" name="table_name" parsley-trigger="change" required=""
                           placeholder="Emri i tavolinës" class="form-control" id="table_name" data-parsley-id="4">
                </div>



                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>
                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}


@section('add-script')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    {{--<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>--}}
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.alert').hide();

            }, 4000);
        });

        //Sweet Alerts warning message
        $('#sa-warning').click(function () {

            row = $("#table-artc-unit").find('tbody tr.focused');
            if (row.size() < 1) {
                alert("Selekto tavolinën");
                return;
            }
            var url = window.location.href;

            tav_id = row[0].id;
            tav_name = $("#"+tav_id).find("td");
            tav_name = $(tav_name[0]).text();

            swal({
                title: "Fshij tavolinën: "+tav_name,
//                text: article_name,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary waves-effect',
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Fshij",
                cancelButtonText: "Jo",
                closeOnConfirm: false
            }, function () {

                $.ajax({

                    type: "delete",

                    url: url + "/" + tav_id,

                    data: {"tav": tav_id, "_token": "{{ csrf_token() }}"},

                    success: function (result) {
                        $("#" + tav_id).remove();
                        swal("Tavolina u fshi me sukses", "", "success");

                    },
                    error: function (result) {
                        swal("Tavolina ka dështuar të fshihet", "", "error");

                    }
                });



            });


        });

        $("#table-artc-unit").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-artc-unit").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });

    </script>

    <script>
        function validateForm(){
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var table_name =  $("#table_name").val();

            if(table_name == "" || table_name.trim() == ""){
                alert(txtAlert);
                $("#table_name").focus();
                return false;
            }

            return true;
        }
    </script>
@stop
