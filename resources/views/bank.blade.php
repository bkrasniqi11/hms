@extends('layouts.app');
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
        <div class="row">
            <div class="col-xs-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                    data-target=".add-article-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                            </button>

                            {{--<button id="modal-edit-article" type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"--}}
                            {{--data-target=".edit-article-modal">--}}
                            {{--<span class="btn-label"><i class="fa fa-pencil"></i>--}}
                            {{--</span>Edito--}}
                            {{--</button>--}}


                            <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                            </button>
                            </p>
                        </div>
                    </div>
                    <br>

                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))

                            <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                        @endif
                    @endforeach

                    <div class="table-rep-plugin">
                        <div class="table-responsive">
                            <table id="table-bank" class="table table-striped table-bordered focus-on">
                                <thead class="thead-default">
                                    <tr>
                                        <th>Banka</th>
                                        <th>Nr.Xhirollogarisë</th>
                                        <th>Swift Code</th>
                                        <th>Statusi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($banks as $bank)
                                    <tr id="{{$bank->id_payment_bank}}" class="unfocused">
                                        <td>{{$bank->payment_bank_name}}</td>
                                        <td>{{$bank->payment_bank_acc_number}}</td>
                                        <td>{{$bank->payment_bank_swift_code}}</td>
                                        <td>Aktive</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- end row -->
@stop

{{--ADD FORM--}}
<div class="modal fade add-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto Artikull </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","action"=>"PaymentBankController@store","method"=>"POST"]) !!}


                <div class="form-group">
                    <label for="bank">Emri<span class="text-danger">*</span></label>
                    <input type="text" name="bank_name" parsley-trigger="change" required="true"
                           placeholder="Emri" class="form-control" id="bank_name" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="acc nr">Nr.Llogarisë<span class="text-danger">*</span></label>
                    <input type="text" name="bank_acc" parsley-trigger="change" required="true"
                           placeholder="Nr.Llogarisë" class="form-control" id="bank_acc" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="swift code">SWIFT Kodi</label>
                    <input type="text" name="bank_swift" parsley-trigger="change" required="true"
                           placeholder="SWIFT Kodi" class="form-control" id="bank_swift" data-parsley-id="4">
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>

                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}

@section('add-script')
    <script>
        <meta name="_token" content="{!! csrf_token() !!}"/>
            <!-- Modal-Effect -->
            <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    {{--<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>--}}
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.alert').hide();

            }, 4000);

            //            $('form').parsley();
        });

        //Sweet Alerts warning message
        $('#sa-warning').click(function () {

            row = $("#table-bank").find('tbody tr.focused');
            if (row.size() < 1) {
                alert("Selekto bankën");
                return;
            }
            var url = window.location.href;

            bank_id = row[0].id;
            bank_name = $("#"+bank_id).find("td");
            bank_name = $(bank_name[0]).text();

            swal({
                title: "Fshij bankën: "+bank_name,
//                text: article_name,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary waves-effect',
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Fshij",
                cancelButtonText: "Jo",
                closeOnConfirm: false
            }, function () {

                $.ajax({

                    type: "delete",

                    url: url + "/" + bank_id,

                    data: {"bank": bank_id, "_token": "{{ csrf_token() }}"},

                    success: function (result) {
                        $("#" + bank_id).remove();
                        swal("Banka u fshi me sukses", "", "success");

                    },
                    error: function (result) {
                        swal("Banka ka dështuar të fshihet", "", "error");

                    }
                });



            });


        });

        $("#table-bank").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-bank").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });

    </script>

    <script>
        function validateForm(){
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var bank_name =  $("#bank_name").val();
            var bank_acc =  $("#bank_acc").val();
            var bank_swift =  $("#bank_swift").val();

            if(bank_name == "" || bank_name.trim() == "" || bank_name.length < 2){
                alert(txtAlert);
                $("#bank_name").focus();
                return false;
            }

            if(!isNaN(bank_name)){
                alert(txtAlert);
                $("#bank_name").focus();
                return false;
            }

            if(bank_acc == "" || bank_acc.trim() == "" || bank_acc.length < 2){
                alert(txtAlert);
                $("#bank_acc").focus();
                return false;
            }

            if(bank_swift == "" || bank_swift.trim() == "" || bank_swift.length < 2){
                alert(txtAlert);
                $("#bank_swift").focus();
                return false;
            }

            return true;
        }
    </script>
@stop