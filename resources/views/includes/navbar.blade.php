<div class="navbar-custom">
    <div class="container">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @if(\Illuminate\Support\Facades\Auth::check())
                <li>
                    <a href="{{ URL::to('/') }}"><i class="fa fa-credit-card"></i> <span> POS </span> </a>
                </li>
                @if($user->id_privilege == 1)
                <li class="has-submenu">
                    <a href="#"><i class="fa fa-plus"></i> <span> Regjistrim </span> </a>
                    <ul class="submenu megamenu">
                        <li>
                            <ul>
                                <li><a href="{{ URL::to('purchase') }}">Blerje</a></li>
                                <hr>
                                <li><a href="{{ URL::to('article-unit') }}">Njësi</a></li>
                                <li><a href="{{ URL::to('article') }}">Artikuj</a></li>
                                <li><a href="{{ URL::to('article-category') }}">Kategori për artikuj</a></li>
                                <hr>
                                <li><a href="{{ URL::to('tables') }}">Tavolinat</a></li>
                                <hr>
                                <li><a href="{{ URL::to('employee ') }}">Punëtor</a></li>
                                <hr>
                                <li><a href="{{ URL::to('partner') }}">Partner</a></li>
                                <li><a href="{{ URL::to('partner-category') }}">Kategori për partner</a></li>
                                <hr>
                                <li><a href="{{ URL::to('bank') }}">Bankë</a></li>

                            </ul>
                        </li>

                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#"><i class="fa fa-file-text"></i> <span> Raporte </span> </a>
                    <ul class="submenu megamenu">
                        <li>
                            <ul>
                                {{--<li><a href="#">Stoku</a></li>--}}
                                {{--<li><a href="#">Lista Artikujve</a></li>--}}
                                {{--<li><a href="#">Lista Blerjeve</a></li>--}}
                                <li><a href="employees-stats">Lista shtijeve për puntorë</a></li>
                                <hr>
                                <li><a href="{{ URL::to('/articles-for-supply') }}">Artikuj për furnizim</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                    @endif
                    @endif
            </ul>
            <!-- End navigation menu  -->
        </div>
    </div>
</div>