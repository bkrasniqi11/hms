<div class="topbar-main">
    <div class="container">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{ URL::to('/index') }}" class="logo">
                <!--<i class="zmdi zmdi-group-work icon-c-logo"></i>-->
                <span>HMS</span>
            </a>
        </div>
        <!-- End Logo container-->


        <div class="menu-extras">

            <ul class="nav navbar-nav pull-right">

                <li class="nav-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span> </span>
                            <span> </span>
                            <span> </span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </li>



                <li class="nav-item dropdown notification-list">
                    <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <img src="assets/images/users/aa.png" alt="user" class="img-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <h5 class="text-overflow" title="{{$user->employee_name}}"><small>Welcome {{$user->employee_name}}</small> </h5>
                                @else
                                <h5 class="text-overflow" title="user"><small>Please LOGIN</small> </h5>
                                @endif
                        </div>

                        <!-- item-->
                        {{--<a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                            {{--<i class="zmdi zmdi-account-circle"></i> <span>Profile</span>--}}
                        {{--</a>--}}

                        <!-- item-->
                        <a href="logout" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-power"></i> <span>Logout</span>
                        </a>

                    </div>
                </li>

            </ul>

        </div> <!-- end menu-extras -->
        <div class="clearfix"></div>

    </div> <!-- end container -->
</div>
