@extends('layouts.app')
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        {{--<a href="javascript:window.print()" class="btn btn-dark waves-effect waves-light"><i class="fa fa-print"></i></a>--}}
                        <button id="print-table" type="button" class="btn btn-default waves-effect waves-light"
                                data-toggle="modal"
                                data-target=".add-article-modal">
                                <span class="btn-label"><i class="fa fa-print"></i>
                                </span>Print
                        </button>

                        </p>
                    </div>
                </div>
                <br>

                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                    @endif
                @endforeach

                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-artc-unit"
                               class="table table-striped table-bordered focus-on table-row-hand">
                            <thead class="thead-default">
                            <tr>
                                <th>Punëtori</th>
                                <th>Data</th>
                                <th>Totali &euro;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empShifts as $empShift)
                                <tr>
                                    <td> {{ ucfirst($empShift->employee->employee_name)}} {{ ucfirst($empShift->employee->employee_surname)}}</td>
                                    <td> {{ $empShift->employee_shift_start_datetime}}</td>
                                    @php
                                        $total =0;
                                    @endphp
                                    <td>   @foreach($empShift->orders as $order)
                                            @foreach($order->orderDetails as $orderDetails)
                                                @php
                                                $od_articles_price =  $orderDetails->article->article_selling_price;
                                                $od_quantity = $orderDetails->quantity;
                                                $total += $od_articles_price*$od_quantity;
                                                @endphp

                                            @endforeach
                                        @endforeach
                                        {{$total}}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

@section('add-script')

    <script>

        $("#table-artc-unit").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-artc-unit").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });


        function printDiv() {
            var divToPrint = document.getElementById('table-artc-unit');
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:1px solid #000;' +
                'padding;0.5em;' +
                'text-align: center;' +
                '}' +
                '#table-artc-unit{' +
                'border-collapse: collapse;' +
                '}' +
                'table{ width: 100%};' +
                '</style><h1 style="text-align: center">Artikujt për furnizim </h1> ';
            htmlToPrint += divToPrint.outerHTML;
            newWin = window.open("");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
        }

        $('#print-table').on('click', function () {
            printDiv();
        })

    </script>
@stop