@extends('layouts.app');
@section("head-scripts")
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
@stop
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                data-target=".add-article-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                        </button>

                        {{--<button id="modal-edit-article" type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"--}}
                        {{--data-target=".edit-article-modal">--}}
                        {{--<span class="btn-label"><i class="fa fa-pencil"></i>--}}
                        {{--</span>Edito--}}
                        {{--</button>--}}


                        <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                        </button>


                        </p>

                    </div>
                </div>
                <br>
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                    @endif
                @endforeach

                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-employees"
                               class="table table-striped table-bordered focus-on table-row-hand">
                            <thead class="thead-default">

                            <tr>
                                <th>Puntori</th>
                                <th>Tel</th>
                                <th>Username</th>
                                <th>Privilegji</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <tr id="{{$employee->id_employee}}" class="unfocused">
                                    <td>{{$employee->employee_name}} {{$employee->employee_surname}} </td>
                                    <td>{{$employee->employee_tel}}</td>
                                    <td>{{$employee->username}}</td>
                                    <td>{{$employee->privilege->privileges_description}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end row -->

@stop
{{--ADD FORM--}}
<div class="modal fade add-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto Artikull </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","action"=>"EmployeesController@store","method"=>"POST", "onsubmit" => "return validateForm()"]) !!}


                <div class="form-group">
                    <label for="FName">Emri<span class="text-danger">*</span></label>
                    <input type="text" name="employee_name" parsley-trigger="change" required=""
                           placeholder="Emri" class="form-control" id="employee_name" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Sname">Mbiemri<span class="text-danger">*</span></label>
                    <input type="text" name="employee_surname" parsley-trigger="change" required=""
                           placeholder="Mbiemri" class="form-control" id="employee_surname" data-parsley-id="6">
                </div>

                <div class="form-group">
                    <label for="Tel">Tel</label>
                    <input type="text" name="employee_tel" parsley-trigger="change" required=""
                           placeholder="Tel" class="form-control" id="employee_tel"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Username">Username<span class="text-danger">*</span></label>
                    <input type="text" name="employee_username" parsley-trigger="change" required=""
                           placeholder="Username" class="form-control" id="employee_username" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="password">Password<span class="text-danger">*</span></label>
                    <input type="password" name="employee_password" parsley-trigger="change" required=""
                           placeholder="Password" class="form-control" id="employee_password"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Privilegjet">Privilegjet<span class="text-danger">*</span></label>
                    <select class="form-control" id="employee_privilege" name="employee_privilege">
                        <option disabled selected>Zgjedh privilegjin</option>
                        @foreach($privileges as $privlege)
                            <option value="{{$privlege->id_privilege}}">{{$privlege->privileges_description}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>
                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}


@section('add-script')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    {{--<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>--}}
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

    <script>

        {{--$("#modal-edit-article").click(function(ev){--}}

            {{--row = $("#table-artc-unit").find('tbody tr.focused');--}}
            {{--if (row.size() < 1) {--}}
                {{--ev.stopPropagation();--}}

                {{--alert("Selekto artikullin");--}}
                {{--return;--}}
            {{--}--}}

            {{--var url = window.location.href;--}}

            {{--article_id = row[0].id;--}}

            {{--$.ajax({--}}

                {{--type: "GET",--}}

                {{--url: url+"/" + article_id,--}}

                {{--data: {"articles": article_id, "_token": "{{ csrf_token() }}"},--}}

                {{--success: function (result) {--}}
                    {{--$("#form-modal-edit-article").attr('action',url+"/" + article_id);--}}
                    {{--$("#form-modal-edit-article #article_name").val(result.article_name);--}}
                    {{--$("#form-modal-edit-article #article_serial_number").val(result.article_serial_number);--}}
                    {{--$("#form-modal-edit-article #id_article_unit").val(result.id_article_unit);--}}
                    {{--$("#form-modal-edit-article #id_article_category").val(result.id_article_category);--}}
                    {{--$("#form-modal-edit-article #article_serial_number").val(result.article_serial_number);--}}
                    {{--$("#form-modal-edit-article #article_tvsh").val(result.article_tvsh);--}}
                    {{--$("#form-modal-edit-article #article_bought_price").val(result.article_bought_price);--}}
                    {{--$("#form-modal-edit-article #article_selling_price").val(result.article_selling_price);--}}
                    {{--$("#form-modal-edit-article #article_quanity").val(result.article_quanity);--}}
                    {{--$("#form-modal-edit-article #article_min_quantity").val(result.article_min_quantity);--}}


                {{--},--}}
                {{--error: function (result) {--}}
                    {{--swal("Provoni përsëri", "", "error");--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}

    </script>

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('.alert').hide();

            }, 4000);

            //            $('form').parsley();
        });

        //Sweet Alerts warning message
        $('#sa-warning').click(function () {

            row = $("#table-employees").find('tbody tr.focused');
            if (row.size() < 1) {
                alert("Selekto punëtorin");
                return;
            }
            var url = window.location.href;

            employee_id = row[0].id;
            employee_name = $("#"+employee_id).find("td");
            employee_name = $(employee_name[0]).text();

            swal({
                title: "Fshij punëtorin: "+employee_name,
//                text: article_name,
                type: "warning",
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary waves-effect',
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Fshij",
                cancelButtonText: "Jo",
                closeOnConfirm: false
            }, function () {

                $.ajax({

                    type: "delete",

                    url: url + "/" + employee_id,

                    data: {"employee": employee_id, "_token": "{{ csrf_token() }}"},

                    success: function (result) {
                        $("#" + employee_id).remove();
                        swal("Punëtori u fshi me sukses", "", "success");

                    },
                    error: function (result) {
                        swal("Punëtori ka dështuar të fshihet", "", "error");

                    }
                });



            });


        });


        $("#table-employees").find('tbody tr').click(function () {

            if ($(this).hasClass('focused')) {
                $(this).removeClass('focused');
                return;
            }

            $("#table-employees").find('tbody tr').removeClass('focused');
            $(this).addClass('focused');
        });

    </script>

    <script>
        function validateForm(){
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var employee_name =  $("#employee_name").val();
            var employee_surname =  $("#employee_surname").val();
            var employee_tel =  $("#employee_tel").val();
            var employee_username =  $("#employee_username").val();
            var employee_password =  $("#employee_password").val();
            var employee_privilege =  $("#employee_privilege").val();

            if(employee_name == "" || employee_name.trim() == "" || employee_name.length < 2 || employee_name.length > 45){
                alert(txtAlert);
                $("#employee_name").focus();
                return false;
            }


            if(!isNaN(employee_name)){
                alert(txtAlert);
                $("#employee_name").focus();
                return false;
            }
            if(employee_surname == "" || employee_surname.trim() == "" || employee_surname.length < 2 || employee_name.length > 45){
                alert(txtAlert);
                $("#employee_surname").focus();
                return false;
            }

            if(!isNaN(employee_surname)){
                alert(txtAlert);
                $("#employee_surname").focus();
                return false;
            }

            if(employee_username == "" || employee_username.trim() == "" || employee_username.length < 2 || employee_name.length > 45){
                alert(txtAlert);
                $("#employee_username").focus();
                return false;
            }

            if(!isNaN(employee_username)){
                alert(txtAlert);
                $("#employee_username").focus();
                return false;
            }

            if(employee_password == "" || employee_password.trim() == "" || employee_password.length < 6 || employee_password.length > 45){
                alert(txtAlert);
                $("#employee_password").focus();
                return false;
            }

            if(employee_privilege == null){
                alert(txtAlert);
                $("#employee_privilege").focus();
                return false;
            }


            return true;
        }
    </script>
@stop