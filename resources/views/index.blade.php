@extends('layouts.app');
@section("head-scripts")
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>
    @stop
    @section('body')

    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))


    </p>

    <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
    @endif
    @endforeach

    <button style="display: none"
            id="payment-modal-btn" type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
            data-target=".add-article-modal">
        <span class="btn-label"><i class="fa fa-plus"></i></span>Shto
    </button>

    <div id="table-orders" class="row">
        <div class="col-xs-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card-box index-content">

                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                    <label class="onoffswitch-label" for="myonoffswitch"></label>
                </div>

                <div id="list-table-orders" class="row">
                    @foreach($tables as $table)

                        @if($table->order_table_free === 1)
                            <div class="order_tables">
                                <button id="{{$table->id_order_table}}"
                                        class="btn btn-success table-btn" title="E lirë"
                                        }}>{{$table->order_table_description}}</button>
                            </div>
                        @else
                            @foreach($orders as $order)
                                @if($order->id_order_table === $table->id_order_table)
                                    <div class="order_tables">
                                        <button id="{{$order->id_order}}"
                                                class="btn btn-danger table-btn"
                                                title="E zënë">{{$table->order_table_description}}</button>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </div>
                <br/>

                <div id="order-orderdetails-menu" class="row">

                    <div class="col-md-5" style="overflow: auto">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h2 id="order-table-name"></h2>
                                <input type="hidden" id="order-id" value="">

                                <hr>

                                <table id="table-order-items" class="table table-striped table-bordered">
                                    <thead class="thead-default">
                                    <th class="text-xs-center"></th>
                                    <th class="text-xs-center">Artikulli</th>
                                    <th class="text-xs-center">Çmimi</th>
                                    <th class="text-xs-center">Sasia</th>
                                    <th class="text-xs-center">Totali</th>
                                    </thead>
                                    <tbody id="order-od-ajax">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div id="article-categories-btn">
                                    <button id="all-articles" class="btn btn-dark btn-active  btn-menu-article">Të gjithë artikujt
                                    </button>
                                    @foreach($article_categories as $article_category)
                                        <button id="{{$article_category->id_article_category}}"
                                                class="btn btn-dark btn-menu-article">{{$article_category->article_category_description}}</button>
                                    @endforeach
                                </div>

                                <hr>

                                <div class="input-group">
                                    <input id="search-articles" type="search" placeholder="Kërko artikuj"
                                           class="form-control">
                                    <span class="input-group-addon">
                                                    <span class="fa fa-search"></span>
                                                </span>
                                </div>

                                <br>

                                <div id="menu-items">
                                    @foreach($articles as $article)
                                        <div class="order_tables">
                                            <button id="{{$article->id_article}}"
                                                    class="btn btn-info table-btn">{{$article->article_name}}</button>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>




    {{--Payment FORM--}}
    <div class="modal fade add-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Pagesa </h4>
                </div>
                <div class="modal-body">

                    {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","method"=>"POST"]) !!}


                    <div class="form-group">
                        <label for="Totali">Totali<span class="text-danger">*</span></label>
                        <input type="text" name="payment_total" parsley-trigger="change" required=""
                               placeholder="Totali" data-a-sign="€ " class="form-control autonumber"
                               id="payment_total"
                               disabled data-parsley-id="4"
                               value="0.00">
                    </div>

                    <div class="form-group">
                        <label for="Pagesa">Pagesa<span class="text-danger">*</span></label>
                        <input type="text" value="0.00" data-a-sign="€ " name="payment_paid"
                               parsley-trigger="change"
                               required=""
                               placeholder="Pagesa" class="form-control autonumber" id="payment_paid"
                               data-parsley-id="6">
                    </div>


                    <div class="form-group">
                        <label for="Kusuri">Kusuri<span class="text-danger">*</span></label>
                        <input disabled type="text" data-a-sign="€ " name="payment_giveback"
                               parsley-trigger="change"
                               required=""
                               placeholder="Kusuri" value="0.00" class="form-control autonumber"
                               id="payment_giveback"
                               data-parsley-id="4">
                    </div>


                    <div class="form-group text-right m-b-0">
                        <button id="payment_execute" class="btn btn-primary waves-effect waves-light" type="button">
                            Paguaj
                        </button>

                    </div>

                    {{ Form::close() }}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{--END OF ADD FORM--}}

@stop

@section('add-script')
    <script src="assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

    <script>
        var DELAY = 400, clicks = 0, timer = null;
        $(document).ready(function () {
            jQuery(function ($) {
                $('.autonumber').autoNumeric('init');
            });
        });

        $("#payment_paid").on("keyup", function (e) {


            var total = $("#payment_total").val();
            var paid = $("#payment_paid").val();


            var change = parseFloat(paid.split("€ ")[1]) - parseFloat(total.split("€ ")[1]);
            $("#payment_giveback").val("€ " + change.toFixed(2));

        });

        $(document).on("click touchstart touch", "#payment_execute", function (e){

            var total = $("#payment_total").val();
            var paid = $("#payment_paid").val();
            var order_id = $("#order-id").val();

            var change = parseFloat(paid.split("€ ")[1]) - parseFloat(total.split("€ ")[1]);
            if(change >= 0){
                var token = $('meta[name="csrf-token"]').attr('content');
                var url = "http://localhost/hms-laravel/public/index/order/payment";
                $.ajax({

                    type: "POST",

                    url: url,

                    data: {"id_order": order_id,"_token":token},
                    success: function (data) {
                        swal({
                                title: "Pagesa përfundoi me sukses",
//                text: article_name,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: 'btn-info',
                                confirmButtonText: "Ok"
                            }, function () {
                            location.reload();
                        });
                    },
                    error: function (data) {
                        alert("Transakcioni ka dështuar provoni përsëri");
                    }
                });
            }else{
                  alert("Nuk është paguar e gjithë shuma");
                }
        });

        $(document).on("click touchstart touch", "#table-orders tbody tr btn", function (e) {

            var order_details_id = $(this).parents().get(1).id;
            var url = "http://localhost/hms-laravel/public/index/order/od/";
            var token = $('meta[name="csrf-token"]').attr('content');

            if (this.id.indexOf("plus") >= 0) {
                var q = $($(this).parents().get(1)).find("#od-quantity").val();
                var p = $($(this).parents().get(1)).find("#od-price").text();

                var changed_q = Number(q) + 1;
                var changed_p = ((Number(p.split("€")[0])) * Number(changed_q)).toFixed(2);


                $.ajax({

                    type: "POST",

                    url: url + "qt",

                    data: {"id_order_detail": order_details_id, "number": 1, "_token": token},
                    error: function (result) {
                        alert("Transakcioni dështoi");
                        return;
                    }
                });
                $($(this).parents().get(1)).find("#od-quantity").val(changed_q);
                $($(this).parents().get(1)).find("#od-total").text(changed_p + " € ");
            }

            if (this.id.indexOf("minus") >= 0) {
                var q = $($(this).parents().get(1)).find("#od-quantity").val();
                var p = $($(this).parents().get(1)).find("#od-price").text();

                if (q == 1 || q == '1') {
                    alert("Sasia më e vogel se 1");
                    return;
                }

                var changed_q = Number(q) - 1;
                var changed_p = ((Number(p.split("€")[0])) * Number(changed_q)).toFixed(2);
                $.ajax({

                    type: "POST",

                    url: url + "qt",

                    data: {"id_order_detail": order_details_id, "number": -1, "_token": token},
                    error: function (result) {
                        alert("Transakcioni dështoi");
                        return;
                    }
                });

                $($(this).parents().get(1)).find("#od-quantity").val(changed_q);
                $($(this).parents().get(1)).find("#od-total").text(changed_p + " € ");

            }

            if (this.id.indexOf("remove") >= 0) {
                $.ajax({
                    type: "POST",

                    url: url + "destory",

                    data: {"id_order_detail": order_details_id, "_token": token},
                    success: function (result) {
                        $("#" + order_details_id).fadeOut();
                    },
                    error: function (result) {
                        alert("Transakcioni dështoi");
                        return;
                    }
                });
            }
        });

        $(document).on("click  touchstart touch", ".btn.btn-danger.table-btn", function (e) {
            clicks++;
            var total = Number(0);
            $("#order-table-name").text(this.innerHTML);
            var output = "";

            //getting order-details with ajax
            var url = "http://localhost/hms-laravel/public/index/order";
            var order_id = this.id;
            $("#order-id").val(order_id);
            if (clicks === 1) {
                timer = setTimeout(function () {
                    if (clicks > 1) {
                        return;
                    }

                    clicks = 0;
                }, DELAY);
            } else {
                total=0;
                $.get(url + '/' + order_id, function (data) {
                    if (data.article.length > 0) {

                        for (i = 0; i < data.article.length; i++) {
                            total += Number((Number(data.quantity[i]) * Number(data.price[i])).toFixed(2));
                            total += 0.00;
//                            $("#payment_total").val(total.toFixed(2));
//                            $("#payment_giveback").val(total.toFixed(2));
                            $("#payment_total").val("€ " + total.toFixed(2));
                            $("#payment_paid").val("€ " + total.toFixed(2));
                            $("#payment_giveback").val("€ 0.00");
                        }
                    }
                });
                $("#payment-modal-btn").trigger("click");
                $("#payment_paid").val("€ " + total.toFixed(2));
                $("#payment-modal").fadeIn();
                clicks = 0;
            }


            $.get(url + '/' + order_id, function (data) {
                if (data.article.length > 0) {

                    for (i = 0; i < data.article.length; i++) {
                        total += Number((Number(data.quantity[i]) * Number(data.price[i])).toFixed(2));

//                                $("#payment_paid").val(total);
                        output += "<tr id='" + data.orderDetails[i] + "'><td class='text-xs-center align-middle'>" +
                            "<btn id='od-remove'  class='btn btn-danger'><i class='fa fa-remove'></i></btn>" +
                            "</td>" +
                            "<td>" + data.article[i] + "</td>" +
                            "<td id='od-price' class='text-xs-center'>" + data.price[i] + "&euro;</td>" +
                            "<td class='text-nowrap'>" +
                            "<btn id='minus-quantity-od' class='btn btn-dark btn-sm'>" +
                            "<i class='fa fa-minus'></i></btn>" +
                            "<input id='od-quantity' disabled type='text' value='" + data.quantity[i] + "' class='form-control'" +
                            "style='width:50px;display: inline;text-align: center'>" +
                            "<btn id='plus-quantity-od' class='btn btn-dark btn-sm'>" +
                            "<i class='fa fa-plus'></i></btn>" +
                            "</td>" +
                            "<td id='od-total' class='text-xs-center'>" + (Number(data.quantity[i]) * Number(data.price[i])).toFixed(2) + " &euro;</td>" +
                            "</tr>";
                        document.getElementById("order-od-ajax").innerHTML = output;
//                                orderDetailsFunctions();
                    };
                } else {
                    document.getElementById("order-od-ajax").innerHTML = "";
                }
            });
        });

        $(document).on("click  touchstart touch", ".btn.btn-success.table-btn", function (e) {
            if ($(this).hasClass("btn-danger")) {
                return;
            }
            $("#order-table-name").text(this.innerHTML);
            var output = "";

            //getting order-details with ajax
            var url = "http://localhost/hms-laravel/public/index";
            var table_id = this.id;
            var order_id;
            var token = $('meta[name="csrf-token"]').attr('content');


            $.ajax({

                type: "POST",

                url: url + "/new",

                data: {"id_order_table": this.id, "_token": token},
                success: function (result) {
                    order_id = result.id_order;
                    $("#order-id").val(order_id);
                    $("button#" + table_id).attr("id", order_id);

                },
                error: function (result) {
                    alert("Transaksioni dështoi");
                    return;
                }
            });

            $(this).attr("class", "btn btn-danger table-btn");
            document.getElementById("order-od-ajax").innerHTML = "";
            //orderDetailsFunctions();
        });

        $(document).on("click touchstart touch", "#menu-items .order_tables button", function () {
            var order_id = $("#order-id").val();
            var url = "http://localhost/hms-laravel/public/index";
            var token = $('meta[name="csrf-token"]').attr('content');
            var output = "";

            $.ajax({

                type: "POST",

                url: url + "/new/order/od/" + order_id,

                data: {"id_order": order_id, "id_article": this.id, "_token": token},
                success: function (data) {
                    console.log("aaa:"+data);
                    if (data.article.length > 0) {
                        for (i = 0; i < data.article.length; i++) {
                            output += "<tr id='" + data.orderDetails[i] + "'><td class='text-xs-center align-middle'>" +
                                "<btn id='od-remove'  class='btn btn-danger'><i class='fa fa-remove'></i></btn>" +
                                "</td>" +
                                "<td>" + data.article[i] + "</td>" +
                                "<td id='od-price' class='text-xs-center'>" + data.price[i] + "&euro;</td>" +
                                "<td class='text-nowrap'>" +
                                "<btn id='minus-quantity-od' class='btn btn-dark btn-sm'>" +
                                "<i class='fa fa-minus'></i></btn>" +
                                "<input id='od-quantity' disabled type='text' value='" + data.quantity[i] + "' class='form-control'" +
                                "style='width:50px;display: inline;text-align: center'>" +
                                "<btn id='plus-quantity-od' class='btn btn-dark btn-sm'>" +
                                "<i class='fa fa-plus'></i></btn>" +
                                "</td>" +
                                "<td id='od-total' class='text-xs-center'>" + (Number(data.quantity[i]) * Number(data.price[i])).toFixed(2) + " &euro;</td>" +
                                "</tr>";
                            document.getElementById("order-od-ajax").innerHTML = output;
                            //orderDetailsFunctions();
                        }
                        ;
                    } else {
                        document.getElementById("order-od-ajax").innerHTML = "";
                    }
                },
                error: function (data) {
                    alert("Ka ndodhur një gabim. Provoni përsëri.")
                }
            });
        });

        $(document).on("keyup", "#search-articles", function () {
            var searchKeyword = $(this).val();
            var category = $(".btn-active").attr("id");
            var output = "";

            var url = "http://localhost/hms-laravel/public/article/name";
            $.ajax({

                type: "GET",

                url: url,

                data: {"searchkeyword": searchKeyword, "category": category},
                success: function (data) {
                    if (data.article_description.length > 0) {
                        for (i = 0; i < data.article_description.length; i++) {
                            output += "<div class='order_tables'>" +
                                "<button id='" + data.id_article[i] + "' class='btn btn-info table-btn'>" + data.article_description[i] + "</button>" +
                                "</div>";
                        }
                        ;
                        document.getElementById("menu-items").innerHTML = output;
                    } else {
                        document.getElementById("menu-items").innerHTML = "";
                    }
                },
                error: function (data) {
                    document.getElementById("menu-items").innerHTML = "";
                    alert("Transakcioni ka dështuar");
                }
            });

        });

        $(document).on("click touchstart touch", "#article-categories-btn button", function () {

            $(this).parent().find('button').removeClass('btn-active');
            $(this).addClass('btn-active');
            var category = this.id;
            var output = "";

            var url = "http://localhost/hms-laravel/public/article/category/";
            $.ajax({

                type: "GET",

                url: url + category,

                data: {"category": category},
                success: function (data) {

                    if (data.article_description.length > 0) {
                        for (i = 0; i < data.article_description.length; i++) {
                            output += "<div class='order_tables'>" +
                                "<button id='" + data.id_article[i] + "' class='btn btn-info table-btn'>" + data.article_description[i] + "</button>" +
                                "</div>";
                        }
                        ;
                        document.getElementById("menu-items").innerHTML = output;
                    } else {
                        document.getElementById("menu-items").innerHTML = "<div class='order_tables'></div>";
                    }
                },
                error: function (data) {
                    document.getElementById("menu-items").innerHTML = "";
                    alert("Transakcioni ka dështuar");
                }
            });
        });
    </script>

    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

@stop