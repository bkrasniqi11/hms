@extends('layouts.app');
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<!-- Plugins css -->
<link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="assets/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-default waves-effect waves-light" data-toggle="modal"
                                data-target=".add-article-modal">
                                <span class="btn-label"><i class="fa fa-plus"></i>
                                </span>Shto
                        </button>

                        <button id="modal-edit-article" type="button" class="btn btn-default waves-effect waves-light"
                                style="display: none"
                                data-toggle="modal"
                                data-target=".edit-article-modal">
                                <span class="btn-label"><i class="fa fa-pencil"></i>
                                </span>Edito
                        </button>


                        <button type="button" class="btn btn-default waves-effect waves-light" id="sa-warning">
                                <span class="btn-label"><i class="fa fa-remove"></i>
                                </span>Fshij
                        </button>
                        </p>
                    </div>
                </div>
                <br>
                {{--<br>--}}

                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <div class="alert alert-{{ $msg }}"><p>{{ Session::get('alert-' . $msg) }}</p></div>
                    @endif
                @endforeach
                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-purchase" class="table table-striped table-bordered focus-on">
                            <thead class="thead-default">
                            <tr>
                                <th>Nr.Fatures</th>
                                <th>Partneri</th>
                                <th>Data</th>
                                <th>Çmimi pa tvsh</th>
                                <th>Zbritje</th>
                                <th>Tvsh</th>
                                <th>Totali</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchases as $purchase)
                                <tr id="{{$purchase->id_purchase}}" class="unfocused">
                                    <td>{{ $purchase->purchase_invoice_number }}</td>
                                    <td>{{  $purchase->partner->partner_name }}</td>
                                    <td>{{  date('d-m-Y', strtotime($purchase->purchase_date)) }}</td>
                                    <td>{{ $purchase->purchase_cost_wtvsh }}</td>
                                    <td>{{ $purchase->purchase_cost_discount }}</td>
                                    @php
                                        $tvsh = $purchase->purchase_cost_tvsh;
                                        $tvsh *= 100;

                                    @endphp
                                    <td>{{ $tvsh }}%</td>
                                    @php
                                        $x = $purchase->purchase_cost_wtvsh;
                                        $x -= $purchase->purchase_cost_discount;
                                        $x *= $purchase->purchase_cost_tvsh;
                                        $x += $purchase->purchase_cost_wtvsh;
                                    @endphp

                                    <td>{{$x}}</td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- end row -->
@stop

{{--ADD FORM--}}
<div class="modal fade add-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Shto Artikull </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","action"=>"PurchaseController@store","method"=>"POST", "onsubmit"=>"return validateForm()"]) !!}


                <div class="form-group">
                    <label for="nrFatures">Nr. Fatures<span class="text-danger">*</span></label>
                    <input type="text" name="purchase_invoice_number" parsley-trigger="change" required=""
                           placeholder="Nr. Fatures" class="form-control" id="purchase_invoice_number"
                           data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Partners">Partneri<span class="text-danger">*</span></label>
                    <select class="form-control" id="purchase_partner" name="purchase_partner">
                        <option disabled selected>Zgjedh Partnerin</option>
                        @foreach($partners as $partner)
                            <option value="{{$partner->id_partner}}">{{$partner->partner_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="date">Data fatures<span class="text-danger">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="dd-mm-yyyy" id="datepicker"
                               name="purchase_datepicker">
                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                    </div>
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Shto
                    </button>

                </div>

                {{ Form::close() }}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF ADD FORM--}}

{{--EDIT FORM--}}
<div class="modal fade edit-article-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel"> Regjistro </h4>
            </div>
            <div class="modal-body">

                {!! Form::open(["data-parsley-validate"=>"","novalidate"=>"","method"=>"POST","id"=>"form-modal-edit-article"]) !!}
                {{--,"action"=>"PurchaseDetailsController@store"--}}

                <div class="form-group">
                    <label for="Njesia">Artikulli</label>
                    <select class="form-control" id="purchase_article" name="purchase_article">
                        <option disabled selected>Zgjedh Artikullin</option>
                        @foreach($articles as $article)
                            <option value="{{$article->id_article}}">{{$article->article_name }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <label for="Quantity">Sasia</label>
                    <input type="text" name="purchased_quantity" parsley-trigger="change" placeholder="Sasia"
                           required="" value="0" class="form-control" id="purchased_quantity" data-parsley-id="4">
                </div>

                <div class="form-group">
                    <label for="Cmimi">Çmimi</label>
                    <input type="text" name="purchased_price" parsley-trigger="change" required=""
                           value="0" placeholder="Çmimi blerës" class="form-control" id="purchased_price"
                           data-parsley-id="4">
                </div>


                <div class="form-group text-right m-b-0">
                    <input type="hidden" id="purchase_id" name="purchase_id" value="0">
                    <button id="btn-add-pd" class="btn btn-primary waves-effect waves-light" type="button">
                        Submit
                    </button>
                </div>

                {{ Form::close() }}

                <br/>
                <br/>
                {{--<p></p>--}}
                <div class="table-rep-plugin">
                    <div class="table-responsive">
                        <table id="table-purchase-details" class="table table-striped table-bordered focus-on">
                            <thead class="thead-default">
                            <tr>
                                <th>Artikulli</th>
                                <th>Sasia</th>
                                <th>Çmimi</th>
                                <th>Totali</th>

                            </tr>
                            </thead>
                            <tbody id="table-body-pd">

                            <tr class="unfocused">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            </tbody>

                        </table>
                    </div>

                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{--END OF EDIT FORM--}}


@section('add-script')
    <script src="assets/plugins/moment/moment.js"></script>
    <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/clockpicker/bootstrap-clockpicker.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script src="assets/pages/jquery.form-pickers.init.js"></script>
    <script>
        var DELAY = 500, clicks = 0, timer = null;
        $("#table-purchase").find('tbody tr').click(function () {
            clicks++;
            var purchase_id = this.id;
            var url = "http://localhost/hms-laravel/public/purchase";
            var token = $('meta[name="csrf-token"]').attr('content');
            var output = "";
            if (clicks === 1) {
                timer = setTimeout(function () {
                    if (clicks > 1) {
                        $("#table-purchase").find('tbody tr').removeClass('focused');
                        $(this).addClass('focused');

                        $.ajax({

                            type: "GET",

                            url: url + "/get/pd/" + purchase_id,

                            data: {"purchase_id": purchase_id, "_token": token},
                            success: function (data) {
                                if (data.article.length > 0) {
                                    document.getElementById("table-body-pd").innerHTML = "";
                                    for (i = 0; i < data.article.length; i++) {
                                        output += "<tr>" +
                                            "<td>" + data.article[i] + "</td>" +
                                            "</tr>";
                                    }
                                    document.getElementById("table-body-pd").innerHTML = output;
                                } else {
                                    document.getElementById("table-body-pd").innerHTML = "<tr class=\"unfocused\"><td></td><td></td><td></td><td></td></tr>";
                                }
                            },
                            error: function (data) {
                                alert("Ka ndodhur një gabim. Provoni përsëri.");
                            }
                        });


                        return;
                    }

                    clicks = 0;
                }, DELAY);

                if ($(this).hasClass('focused')) {
                    $(this).removeClass('focused');
                    return;
                }

                $("#table-purchase").find('tbody tr').removeClass('focused');
                $(this).addClass('focused');

            } else {
                $("#modal-edit-article").trigger("click");
                $("#purchase_id").val(this.id);
                $("#table-purchase").find('tbody tr').removeClass('focused');
                $(this).addClass('focused');
                $.ajax({

                    type: "GET",

                    url: url + "/get/pd/" + purchase_id,

                    data: {"purchase_id": purchase_id, "_token": token},
                    success: function (data) {
                        if (data.article.length > 0) {
                            document.getElementById("table-body-pd").innerHTML = "";
                            for (i = 0; i < data.article.length; i++) {
                                output += "<tr>" +
                                    "<td>" + data.article[i] + "</td>" +
                                    "<td>" + data.price[i] + "</td>" +
                                    "<td>" + data.quantity[i] + "</td>" +
                                    "<td>" + ((Number(data.price[i])) * (Number(data.quantity[i]))).toFixed(2);
                                +"</td>" +
                                "</tr>";
                            }
                            document.getElementById("table-body-pd").innerHTML = output;
                        } else {
                            document.getElementById("table-body-pd").innerHTML = "<tr class=\"unfocused\"><td></td><td></td><td></td><td></td></tr>";
                        }
                    },
                    error: function (data) {
                        alert("Ka ndodhur një gabim. Provoni përsëri.");
                    }
                });
                clicks = 0;
            }

        });

        $("#btn-add-pd").on("click  touchstart touch", function (e) {
            if(!validateForm2()){
                return false;
            }

            var purchase_id = $("#purchase_id").val();
            var article_id = $("#purchase_article").val();
            var quantity = $("#purchased_quantity").val();
            var price = $("#purchased_price").val();
            var url = "http://localhost/hms-laravel/public/purchase";
            var token = $('meta[name="csrf-token"]').attr('content');
            var output = "";

            $.ajax({

                type: "POST",

                url: url + "/new/pd/" + purchase_id,

                data: {
                    "purchase_id": purchase_id,
                    "id_article": article_id,
                    "quantity": quantity,
                    "price": price,
                    "_token": token
                },
                success: function (data) {
                    $("#purchased_quantity").val("");
                    $("#purchased_price").val("");
                    $("#purchase_article").prop('selectedIndex', 0);

                    if (data.article.length > 0) {
                        document.getElementById("table-body-pd").innerHTML = "";
                        for (i = 0; i < data.article.length; i++) {
                            output += "<tr>"
                                + "<td>" + data.article[i] + "</td>"
                                + "<td>" + data.price[i] + "</td>"
                                + "<td>" + data.quantity[i] + "</td>"
                                + "<td>" + ((Number(data.price[i])) * (Number(data.quantity[i]))).toFixed(2);
                            +"</td>"
                            + "</tr>"
                        }
                        document.getElementById("table-body-pd").innerHTML = output;
                    } else {
                        document.getElementById("table-body-pd").innerHTML = "<tr class=\"unfocused\"><td></td><td></td><td></td><td></td></tr>";
                    }
                },
                error: function (data) {
                    alert("Ka ndodhur një gabim. Provoni përsri")
                }
            });
        });
    </script>


    <script>
        function validateForm() {
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var purchase_invoice_number = $("#purchase_invoice_number").val();
            var purchase_partner = $("#purchase_partner").val();
            var datepicker = $("#datepicker").val();

            if (purchase_invoice_number == "" || purchase_invoice_number.trim() == "") {
                alert(txtAlert);
                $("#purchase_invoice_number").focus();
                return false;
            }

            if (purchase_partner == null) {
                alert(txtAlert);
                $("#purchase_partner").focus();
                return false;
            }

            if (datepicker == "" || datepicker.trim() == "") {
                alert(txtAlert);
                $("#datepicker").focus();
                return false;
            }

        }


        function validateForm2() {
            var txtAlert = "Gabim gjatë shtypjes së të dhënave!";
            var purchased_quantity = $("#purchased_quantity").val();
            var purchased_price = $("#purchased_price").val();
            var purchase_article = $("#purchase_article").val();

            if (purchase_article == null) {
                alert(txtAlert);
                $("#purchase_article").focus();
                return false;
            }

            if (purchased_quantity == "" || purchased_quantity.trim() == "") {
                alert(txtAlert);
                $("#purchased_quantity").focus();
                return false;
            }

            if (isNaN(purchased_quantity)) {
                alert(txtAlert);
                $("#purchased_quantity").focus();
                return false;
            }
            if (Number(purchased_quantity) < 1) {
                alert(txtAlert);
                $("#purchased_quantity").focus();
                return false;
            }

            if (purchased_price == "" || purchased_price.trim() == "") {
                alert(txtAlert);
                $("#purchased_price").focus();
                return false;
            }

            if (isNaN(purchased_price)) {
                alert(txtAlert);
                $("#purchased_price").focus();
                return false;
            }
            if (Number(purchased_price) < 0.01) {
                alert(txtAlert);
                $("#purchased_price").focus();
                return false;
            }
            return true;
        }
    </script>
@stop