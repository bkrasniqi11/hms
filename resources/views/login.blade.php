<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="HM-SYSTEM BY BK">
        <meta name="author" content="Bardh Krasniqi">

        <!-- App Favicon -->
        {{--<link rel="shortcut icon" href="images/favicon.ico">--}}

        <!-- App title -->
        <title>HM-System</title>

        <!-- App CSS -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">

        	<div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <a href="index.html" class="logo">
                            {{--<i class="zmdi zmdi-group-work icon-c-logo"></i>--}}
                            <span>HORECA Managment System</span>
                        </a>
                    </div>
                    <div class="m-t-30 m-b-20">
                        <div class="col-xs-12 text-xs-center">
                            <h6 class="text-muted text-uppercase m-b-0 m-t-0">Sign In</h6>
                        </div>

                            @if ($errors->has('username'))
                                <br>
                                <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif


                        <form class="form-horizontal m-t-20" onsubmit="return validateFrom()" role="form" method="POST" action="{{route('login.check')}}">
                            {{ csrf_field() }}
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="username" id="username"  placeholder="Username">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" name="password" id="password" placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <div class="checkbox checkbox-custom">
                                        <input id="checkbox-signup" type="checkbox" name="remember">
                                        <label for="checkbox-signup">
                                            Remember me
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group text-center m-t-30">
                                <div class="col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->

        </div>
        <!-- end wrapper page -->


        <script>
            var resizefunc = [];
            function validateFrom(){
                var username =  $("#username").val();
                var password =  $("#password").val();
                var txtAlert = "Gabim gjatë shtypjes së të dhënave!";

                if(username == "" || username.trim() == ""){
                    alert(txtAlert);
                    $("#username").focus();
                    return false;
                }

                if(!isNaN(username)){
                    alert(txtAlert);
                    $("#username").focus();
                    return false;
                }

                if(password == "" || password.trim() == ""){
                    alert(txtAlert);
                    $("#password").focus();
                    return false;
                }

            }
        </script>

        <!-- jQuery  -->
        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>