<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('login/check','UserController@checklogin')->name("login.check");
Route::get('logout', 'UserController@logout')->name("logout");
Route::get('login', 'UserController@index')->name('login');
Route::get('index/order/{order_id}', 'OrdersController@getOrder');
Route::post('index/new', 'OrdersController@newOrder');
Route::post('index/new/order/od/{order_id}', 'OrdersController@newOrderDetails');
Route::post('index/order/payment', 'OrdersController@orderPayment');
Route::post('index/order/od/destory', 'OrderDetailsController@destroy');
Route::post('index/order/od/qt', 'OrderDetailsController@updateQuantity');
Route::resource('/', 'OrdersController');
Route::resource('index', 'OrdersController');

Route::resource('article-category', 'ArticleCategoryController');

Route::resource('article-unit','ArticleUnitController' );

Route::resource('tables','OrderTablesController');

Route::get('article/category/{category}', 'ArticleController@getArticlesByCatgeory');
Route::get('article/name', 'ArticleController@getArticlesByName');
Route::get('articles-for-supply','ArticleController@articleforsupply');
Route::resource('article', 'ArticleController');

Route::resource('bank','PaymentBankController');

Route::get('employees-stats','EmployeesController@empStats');
Route::resource('employee', 'EmployeesController');


Route::resource('partner', 'PartnersController');

Route::resource('partner-category', 'PartnersCategoryController');

Route::resource('purchase', 'PurchaseController');

Route::get('purchase/get/pd/{id_purchase}','PurchaseDetailsController@getPurchase');
Route::post('purchase/new/pd/{id_purchase}','PurchaseDetailsController@store');
Route::post('employee/shift','EmployeesController@createShift');



