<?php

namespace Tests\Feature;


use App\Http\Controllers\ArticleCategoryController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ArticleUnitController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PartnersCategoryController;
use App\Http\Controllers\PartnersController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\PurchaseDetailsController;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;


class TestClass extends TestCase
{
    public function testBasicTest()
    {
        // Authenticate
        Auth::guard()->attempt(['username' => "bk", 'password' => "123456"]);
        $user = Auth::user();

        // ---Testimi i metodave---

        //OrderController
        $odCon =  new OrdersController;
        $odCon->getOrder(1);

        //ArticleController
        $artCon =  new ArticleController;
        $artCon->articleforsupply();
        $artCon->show(1);
        $artCon->reset_ai_article();

        //ArticleUnitController
        $artUnitCon =  new ArticleUnitController();
        $artUnitCon->index();

        //ArticleCategoryController
        $artCatCon = new ArticleCategoryController();
        $artCatCon->index();

        //EmloyeesController
        $empCon =  new EmployeesController;
        $empCon->index();
        $empCon->empStats();
        $empCon->checkShift($user);
        $empCon->getShift($user);

        //PartnersCategoryController
        $partCatCon =  new PartnersCategoryController();
        $partCatCon->index();

        //PartnersController
        $partCon =  new PartnersController();
        $partCon->index();

        //PurchaseController
        $purchCon =  new PurchaseController();
        $purchCon->index();

        //PurchaseDetailsController
        $purchDetCon = new PurchaseDetailsController();
        $purchDetCon->getPurchase(1);

        // ---Testimi i path's---
        $response = $this->get('/');
        $response1 = $this->get('/index');
        $response2 = $this->get('/article');
        $response3 = $this->get('/purchase');
        $response4 = $this->get('/article-unit');
        $response5 = $this->get('/article-category');
        $response6 = $this->get('/tables');
        $response7 = $this->get('/employee');
        $response8 = $this->get('/partner');
        $response9 = $this->get('/partner-category');
        $response10 = $this->get('/bank');
        $response11 = $this->get('/employees-stats');
        $response12 = $this->get('/articles-for-supply');

        $response->assertStatus(200);
        $response1->assertStatus(200);
        $response2->assertStatus(200);
        $response3->assertStatus(200);
        $response4->assertStatus(200);
        $response5->assertStatus(200);
        $response6->assertStatus(200);
        $response7->assertStatus(200);
        $response8->assertStatus(200);
        $response9->assertStatus(200);
        $response10->assertStatus(200);
        $response11->assertStatus(200);
        $response12->assertStatus(200);
    }
}
