<?php

namespace App\Http\Controllers;

use App\ArticleCategory;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ArticleCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $article_categories = ArticleCategory::all();
            return view('article_category', compact('user', 'article_categories'));
        } else {
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'aritcle_category_description' => 'required|string',
        ]);
        $new_article_category = new ArticleCategory();
        $new_article_category->article_category_description = $request->aritcle_category_description;

        try {
            $new_article_category->save();
            $request->session()->flash('alert-success', 'Kategoria u shtua me sukses');
            return redirect()->route("article-category.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Kategoria nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleCategory $articleCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleCategory $articleCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleCategory $articleCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArticleCategory $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($articleCategory)
    {
        try {
            ArticleCategory::destroy($articleCategory);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $articleCategory->session()->flash('alert-danger', 'Njësia nuk u largua. Provoni përsëri !');
            return redirect()->back();

        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `article_category` auto_increment = 1;');
    }
}
