<?php

namespace App\Http\Controllers;

use App\OrderNTables;
use Illuminate\Http\Request;

class OrderNTablesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderNTables  $orderNTables
     * @return \Illuminate\Http\Response
     */
    public function show(OrderNTables $orderNTables)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderNTables  $orderNTables
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderNTables $orderNTables)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderNTables  $orderNTables
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderNTables $orderNTables)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderNTables  $orderNTables
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderNTables $orderNTables)
    {
        //
    }

    public static function ntOrder($id_order){
        $ntablesO = OrderNTables::where("id_order",$id_order);
        return $ntablesO;
    }
}
