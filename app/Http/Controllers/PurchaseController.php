<?php

namespace App\Http\Controllers;

use App\Articles;
use App\Partners;
use App\Purchase;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $purchases = Purchase::all();
            $partners = Partners::all();
            $articles = Articles::all();
            return view('purchase', compact('purchases', 'user', 'partners', 'articles'));
        } else {
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'purchase_invoice_number' => 'required',
            'purchase_datepicker' => 'required',
            'purchase_partner' => 'required',
        ]);


        $new_purchase = new Purchase;
        $new_purchase->purchase_invoice_number = $request->purchase_invoice_number;
        $date = date('Y-m-d', strtotime($request->purchase_datepicker));
        $new_purchase->id_partner = $request->purchase_partner;
        $new_purchase->purchase_date = $date;

        try {
            $saved = $new_purchase->save();
            $request->session()->flash('alert-success', 'Blerja u regjitstrua me sukses !');
            return redirect()->route("purchase.index");
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Blerja nuk u regjistrua. Provoni përsëri!');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `articles` auto_increment = 1;');
    }
}
