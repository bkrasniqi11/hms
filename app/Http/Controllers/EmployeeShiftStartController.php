<?php

namespace App\Http\Controllers;

use App\EmployeeShiftStart;
use Illuminate\Http\Request;

class EmployeeShiftStartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeShiftStart  $employeeShiftStart
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeShiftStart $employeeShiftStart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeShiftStart  $employeeShiftStart
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeShiftStart $employeeShiftStart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeShiftStart  $employeeShiftStart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeShiftStart $employeeShiftStart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeShiftStart  $employeeShiftStart
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeShiftStart $employeeShiftStart)
    {
        //
    }
}
