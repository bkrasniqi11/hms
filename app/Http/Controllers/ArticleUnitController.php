<?php

namespace App\Http\Controllers;

use App\ArticleUnit;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ArticleUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $article_units = ArticleUnit::all();
            return view('article_unit', compact('user', 'article_units'));
        } else {
            return redirect('/index');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'aritcle_unit_description' => 'required|string',
        ]);
        $new_article_unit = new ArticleUnit;
        $new_article_unit->article_unit_description = $request->aritcle_unit_description;

        try {
            $new_article_unit->save();
            $request->session()->flash('alert-success', 'Njësia u shtua me sukses');
            return redirect()->route("article-unit.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Njësia nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArticleUnit $articleUnit
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleUnit $articleUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArticleUnit $articleUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleUnit $articleUnit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ArticleUnit $articleUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleUnit $articleUnit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArticleUnit $articleUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($articleUnit)
    {
        try {
            ArticleUnit::destroy($articleUnit);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $articleUnit->session()->flash('alert-danger', 'Njësia nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `article_unit` auto_increment = 1;');
    }
}
