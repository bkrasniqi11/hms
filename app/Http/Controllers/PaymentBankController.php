<?php

namespace App\Http\Controllers;

use App\PaymentBank;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PaymentBankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $banks = PaymentBank::all();
            return view('bank', compact('user', 'banks'));
        } else {
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                "bank_name" => "required|string",
                "bank_acc" => "required",
                "bank_swift" => "required",
            ]);

            $new_bank = new  PaymentBank;
            $new_bank->payment_bank_name = $request->bank_name;
            $new_bank->payment_bank_acc_number = $request->bank_acc;
            $new_bank->payment_bank_swift_code = $request->bank_swift;


            $new_bank->save();
            $request->session()->flash('alert-success', 'Banka u shtua me sukses');
            return redirect()->route("bank.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Banka nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentBank $paymentBank
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentBank $paymentBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentBank $paymentBank
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentBank $paymentBank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\PaymentBank $paymentBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentBank $paymentBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentBank $paymentBank
     * @return \Illuminate\Http\Response
     */
    public function destroy($paymentBank)
    {
        try {
            PaymentBank::destroy($paymentBank);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $paymentBank->session()->flash('alert-danger', 'Banka nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `payment_bank` auto_increment = 1;');
    }
}
