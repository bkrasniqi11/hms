<?php

namespace App\Http\Controllers;

use App\City;
use App\Partners;
use App\PartnersCategory;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PartnersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $citys = City::all();
            $partner_categorys = PartnersCategory::all();
            $partners = Partners::all();
            return view('partners', compact('user', 'citys', 'partner_categorys', 'partners'));
        }else{
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                "partner_name" => "required|string",
                "partner_category" => "required",
                "partner_city" => "required",
                "partner_fiscal" => "required",
                "partner_business" => "required|string",
                "partner_tvsh" => "required|string",
                "partner_adress" => "required|string",
                "partner_tel" => "required",
                "partner_email" => "required|string",
            ]);
        $new_partner =  new Partners();

        $new_partner->partner_name = $request->partner_name;
        $new_partner->id_partner_category = $request->partner_category;
        $new_partner->id_city = $request->partner_city;
        $new_partner->partner_fiscal_number = $request->partner_fiscal;
        $new_partner->partner_bussines_number = $request->partner_business;
        $new_partner->partner_tvsh = $request->partner_tvsh;
        $new_partner->partner_address = $request->partner_adress;
        $new_partner->partner_tel = $request->partner_tel;
        $new_partner->partner_email = $request->partner_email;
        $new_partner->partner_web = $request->partner_web;
        $new_partner->partner_description = $request->partner_desc;
        $new_partner->partner_status = 1;


            $new_partner->save();
            $request->session()->flash('alert-success', 'Partneri u shtua me sukses');
            return redirect()->route("partner.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Partneri nuk u shtua. Provoni përsëri!');
            return redirect()->back();

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function show(Partners $partners)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function edit(Partners $partners)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partners $partners)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function destroy($partner)
    {
        try {
            Partners::destroy($partner);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $partner->session()->flash('alert-danger', 'Partneri nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `partners` auto_increment = 1;');
    }
}
