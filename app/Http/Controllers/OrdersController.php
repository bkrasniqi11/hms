<?php

namespace App\Http\Controllers;

use App\ArticleCategory;
use App\Employees;
use App\EmployeeShiftStart;
use Illuminate\Support\Facades\DB;
use Input;
use App\Articles;
use App\OrderDetails;
use App\Orders;
use App\OrderTables;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\Http\Controllers\EmployeesController;
use Auth;

class OrdersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

            $shift = app('App\Http\Controllers\EmployeesController')->checkShift($user);
//        $empCon =  new EmployeesController;
//            $shift = $empCon->checkShift($user);

            if ($shift != 1) {
                return view('shift');
            }


            $article_categories = ArticleCategory::all();
            $articles = Articles::orderBy('article_name', 'asc')->get();
            $tables = OrderTables::all();
            $orders = Orders::where('id_order_status', 1)->get();

            return view('index', compact('orders', 'articles', 'tables', 'article_categories', 'user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function orderPayment(Request $request)
    {
        $id_order = $request->id_order;

        $order = Orders::find($id_order);
        $order->id_order_status = 2;
        $id_emp_shift = app('App\Http\Controllers\EmployeesController')->getShift(Auth::user());
        $order->id_employee_shift_start_payment = $id_emp_shift[0]->id_employee_shift_start;
        $id_order_table = $order->id_order_table;

        try {
            $order->save();
            OrderTables::find($id_order_table)->update(['order_table_free' => 1]);

            return response()->json($order);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function newOrder(Request $request)
    {
        $id_order_table = $request->id_order_table;
//        $id_employee_shift_start = $request->id_employee_shift_start;
        $new_order = new Orders();

        $id_emp_shift = app('App\Http\Controllers\EmployeesController')->getShift(Auth::user());
        $new_order->id_employee_shift_start = $id_emp_shift[0]->id_employee_shift_start;
        $new_order->id_order_table = $id_order_table;

        try {
            $saved = $new_order->save();
            OrderTables::find($id_order_table)->update(['order_table_free' => 2]);

            return response()->json($new_order);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function newOrderDetails(Request $request)
    {
        $id_order = $request->id_order;
        $id_article = $request->id_article;


        $orderDetails= OrderDetails::select(DB::raw('*'))->where("id_order","=",$id_order)->Where("id_article","=",$id_article)->get();

        if(count($orderDetails) !=0){
            $id_order_detail = $orderDetails[0]->id_order_details;

            $orderDetail = OrderDetails::findOrfail($id_order_detail);
            $od_quantity = $orderDetail->quantity;
            $od_quantity = $od_quantity + 1;
            $orderDetail->quantity = $od_quantity;
            $orderDetail->save();
            $article = Articles::find($id_article);
            $x = $article->article_quanity;
            $article->article_quanity = $x - 1;
            $article->save();
            return $this->getOrder($id_order);
        }

        $new_orderDetails = new OrderDetails();
        $new_orderDetails->quantity = 1;
        $new_orderDetails->id_order = $id_order;
        $new_orderDetails->id_article = $id_article;

        $article = Articles::find($id_article);
        $x = $article->article_quanity;
        $article->article_quanity = $x - 1;

        try {
            $new_orderDetails->save();
            $article->save();
            return $this->getOrder($id_order);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function store(Request $request)
    {
        $new_item = new OrderDetails();
        $new_item->id_order = $request->id_order;
        $new_item->id_article = $request->id_article;
        $new_item->quantity = $request->quantity;

        try {
            $saved = $new_item->save();
            return $this->getOrder($new_item->id_order);
        } catch (Exception $e) {
            return $e;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orders $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orders $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Orders $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orders $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }

    public function getOrder($order)
    {

        $orderDetails = OrderDetails::where('id_order', $order)->get();
        $orderDetail = array();
        $article = array();
        $price = array();
        $quanity = array();
        foreach ($orderDetails as $orD) {
            array_push($orderDetail, $orD->id_order_details);
            array_push($article, $orD->article->article_name);
            array_push($price, $orD->article->article_selling_price);
            array_push($quanity, $orD->quantity);
        }


        return response()->json(array("orderDetails" => $orderDetail, "article" => $article, "price" => $price, "quantity" => $quanity));

    }
}
