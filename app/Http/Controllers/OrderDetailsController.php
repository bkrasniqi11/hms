<?php

namespace App\Http\Controllers;

use App\OrderDetails;
use App\Articles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderDetails $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderDetails $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\OrderDetails $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderDetails $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $orderDetails = OrderDetails::destroy($request->id_order_detail);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
        }
    }

    public function updateQuantity(Request $request)
    {
        $orderDetail = OrderDetails::findOrfail($request->id_order_detail);
        $number = $request->number;
        $od_quantity = $orderDetail->quantity;
        $od_quantity = $od_quantity + $number;
        $orderDetail->quantity = $od_quantity;
        $id_article = $orderDetail->id_article;

        $article = Articles::find($id_article);
        $x = $article->article_quanity;
        if ($number == 1) {
            $article->article_quanity = $x - 1;
        } else {
            $article->article_quanity = $x + 1;
        }
        $saved = $orderDetail->save();
        $article->save();
        return response()->json($saved);
    }

    private
    function reset_ai_article()
    {
        DB::statement('ALTER TABLE `order_details` auto_increment = 1;');
    }
}
