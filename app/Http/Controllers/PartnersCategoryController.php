<?php

namespace App\Http\Controllers;

use App\PartnersCategory;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PartnersCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
        $partnersCategory = PartnersCategory::all();
        return view('partners_category', compact('user','partnersCategory'));
        } else {
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                "pc_description" => "required|string",
            ]);

        $new_pc =  new  PartnersCategory;
        $new_pc->partner_category_description = $request->pc_description;

            $new_pc->save();
            $request->session()->flash('alert-success', 'Kategoria u shtua me sukses');
            return redirect()->route("partner-category.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Kategoria nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PartnersCategory  $partnersCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PartnersCategory $partnersCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PartnersCategory  $partnersCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PartnersCategory $partnersCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PartnersCategory  $partnersCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PartnersCategory $partnersCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PartnersCategory  $partnersCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($partnersCategory)
    {
        try {
            PartnersCategory::destroy($partnersCategory);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $partnersCategory->session()->flash('alert-danger', 'Njësia nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `partner_category` auto_increment = 1;');
    }
}
