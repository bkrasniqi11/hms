<?php

namespace App\Http\Controllers;

use App\OrderTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrderTablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = OrderTables::orderBy('order_table_description')->get();
        $user = Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            return view('tables', compact('tables', 'user'));
        } else {
            return redirect('/index');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "table_name" => "required",
        ]);

        $new_table = new OrderTables;
        $new_table->order_table_description = $request->table_name;

        try {
            $new_table->save();
            $request->session()->flash('alert-success', 'Tavolina u shtua me sukses !');
            return redirect()->route("tables.index");

        } catch (\Exception $ex) {
            $this::reset_ai_table();
            $request->session()->flash('alert-danger', 'Tavolina nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderTables $orderTables
     * @return \Illuminate\Http\Response
     */
    public function show(OrderTables $orderTables)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderTables $orderTables
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderTables $orderTables)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\OrderTables $orderTables
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderTables $orderTables)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderTables $orderTables
     * @return \Illuminate\Http\Response
     */
    public function destroy($orderTables_id)
    {
        try {
            $table = OrderTables::destroy($orderTables_id);
            $this::reset_ai_table();
        } catch (\Exception $ex) {
            $this::reset_ai_table();
            $orderTables_id->session()->flash('alert-danger', 'Tavolina nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_table()
    {
        DB::statement('ALTER TABLE `order_tables` auto_increment = 1;');
    }
}
