<?php

namespace App\Http\Controllers;

use App\Articles;
use App\ArticleUnit;
use App\ArticleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  Auth::user();
        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $article_categories = ArticleCategory::all();
            $article_units = ArticleUnit::all();
//        $articles = Articles::all();
            $articles = Articles::orderBy('article_name')->get();

            return view('article', compact('article_categories', 'article_units', 'articles', 'user'));
        }else {
            return redirect('/index');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function articleforsupply(){
        $user =  Auth::user();
        $article_categories = ArticleCategory::all();
        $article_units = ArticleUnit::all();
        $articles = Articles::whereRaw("article_quanity <= article_min_quantity")->orderBy('article_name')->get();;

        return view('article_supply', compact('article_categories', 'article_units', 'articles','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'article_name' => 'required|string',
            'id_article_unit' => 'required|number',
            'id_article_category' => 'required|number',
            'article_serial_number' => 'required',
            'article_tvsh' => 'required',
            'article_bought_price' => 'required',
            'article_selling_price' => 'required',
            'article_quanity' => 'required',
            'article_min_quantity' => 'required',
        ]);


        $new_article = new Articles;
        $new_article->article_name = $request->article_name;
        $new_article->id_article_unit = $request->id_article_unit;
        $new_article->id_article_category = $request->id_article_category;
        $new_article->article_serial_number = $request->article_serial_number;
        $new_article->article_tvsh = $request->article_tvsh;
        $new_article->article_bought_price = $request->article_bought_price;
        $new_article->article_selling_price = $request->article_selling_price;
        $new_article->article_quanity = $request->article_quanity;
        $new_article->article_min_quantity = $request->article_min_quantity;

        try {
            $saved = $new_article->save();
            $request->session()->flash('alert-success', 'Artikulli u shtua me sukses !');
            return redirect()->route("article.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Artikulli nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Articles $articles
     * @return \Illuminate\Http\Response
     */
    public function show($aricle_id)
    {
        $article = Articles::findOrfail($aricle_id);

        return \Response::json($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Articles $articles
     * @return \Illuminate\Http\Response
     */
    public function edit(Articles $articles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Articles $articles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$aricle_id)
    {
        try {
//            $this->validate($request, [
//                'article_name' => 'required|string',
//                'id_article_unit' => 'required|number',
//                'id_article_category' => 'required|number',
//                'article_serial_number' => 'required',
//                'article_tvsh' => 'required',
//                'article_bought_price' => 'required',
//                'article_selling_price' => 'required',
//                'article_quanity' => 'required',
//                'article_min_quantity' => 'required',
//            ]);

            $article_update = Articles::findOrfail($aricle_id);
            $article_update->update($request->all());

            $request->session()->flash('alert-success', 'Artikulli u ndryshua me sukses!');
            return redirect()->route("article.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Artikulli nuk u ndryshua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**x
     * Remove the specified resource from storage.
     *
     * @param  \App\Articles $articles
     * @return \Illuminate\Http\Response
     */
    public function destroy($aricle_id)
    {
        try {
            $article = Articles::destroy($aricle_id);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $aricle_id->session()->flash('alert-danger', 'Artikulli nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    /**
     *
     */
    public function getArticlesByName(Request $request)
    {
        $searchkeyword = $request->searchkeyword;
        $category = $request->category;

        if($searchkeyword == "" || $searchkeyword == null){
            if($category == "all-articles") {
                $articles = Articles::orderBy('article_name', 'asc')->get();
            }else{
                $articles = Articles::where('article_name',"like", "%".$searchkeyword."%")->Where("id_article_category","=",$category)->orderBy('article_name', 'asc')->get();
            }
        }else{
            if($category == "all-articles"){
                $articles = Articles::where('article_name',"like", "%".$searchkeyword."%")->orderBy('article_name', 'asc')->get();
            }else{
                $articles = Articles::where('article_name',"like", "%".$searchkeyword."%")->Where("id_article_category","=",$category)->orderBy('article_name', 'asc')->get();
            }
        }
        $id_article  = array();
        $article_description  = array();
        $article_description1  = array();

        foreach ($articles as $article){
            array_push($id_article,$article->id_article);
            array_push($article_description,$article->article_name);
        }
        array_push($article_description1, $category);

        return  response()->json(array("id_article"=>$id_article,"article_description"=>$article_description,"article_description1"=>$article_description1));
    }

    /**
     *
     */
    public function getArticlesByCatgeory($category)
    {
        if($category == "all-articles"){
            $articles =  Articles::orderBy('article_name','asc')->get();
        }else{
            $articles = Articles::where('id_article_category', $category)->orderBy('article_name', 'asc')->get();
        }
        $id_article  = array();
        $article_description  = array();

        foreach ($articles as $article){
            array_push($id_article,$article->id_article);
            array_push($article_description,$article->article_name);
        }

        return  response()->json(array("id_article"=>$id_article,"article_description"=>$article_description));
    }


    /**
     *
     */
    public function reset_ai_article()
    {
        DB::statement('ALTER TABLE `articles` auto_increment = 1;');
    }
}
