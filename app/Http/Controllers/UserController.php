<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class UserController extends Controller
{
    use ThrottlesLogins;
//
    public function __construct()
    {
        $this->middleware('guest:web', ['except' => ['logout']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    public function checklogin(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard()->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {
            return redirect('index');
        }
        $this->incrementLoginAttempts($request);

        return redirect('login');
    }





    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        return redirect('login');
    }
}