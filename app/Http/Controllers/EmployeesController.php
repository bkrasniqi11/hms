<?php

namespace App\Http\Controllers;

use App\Employees;
use App\EmployeeShiftStart;
use App\Privileges;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $emp_privilege = $user->id_privilege;
        if ($emp_privilege == 1) {
            $employees = Employees::all();
            $privileges = Privileges::all();
            return view('employees', compact('employees', 'user', 'privileges'));
        } else {
            return redirect('/index');
        }
    }

    public function empStats()
    {
        $user = Auth::user();
        $empShifts = EmployeeShiftStart::all();


        return view('employee-stats', compact('employees', 'user','empShifts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
        $this->validate($request, [
            "employee_name" => "required|string",
            "employee_surname" => "required|string",
            "employee_tel" => "required",
            "employee_username" => "required|string",
            "employee_password" => "required",
            "employee_privilege" => "required",

        ]);
        $new_employee = new Employees;
        $new_employee->employee_name = $request->employee_name;
        $new_employee->employee_surname = $request->employee_surname;
        $new_employee->employee_tel = $request->employee_tel;
        $new_employee->username = $request->employee_username;
        $new_employee->password = Hash::make($request->employee_password);
        $new_employee->id_privilege = $request->employee_privilege;


            $new_employee->save();
            $request->session()->flash('alert-success', 'Punetori u shtua me sukses !');
            return redirect()->route("employee.index");

        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $request->session()->flash('alert-danger', 'Punetori nuk u shtua. Provoni përsëri!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Employees $employees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees $employees
     * @return \Illuminate\Http\Response
     */
    public function edit(Employees $employees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Employees $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employees $employees)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($employee)
    {
        try {
            Employees::destroy($employee);
            $this::reset_ai_article();
        } catch (\Exception $ex) {
            $this::reset_ai_article();
            $employee->session()->flash('alert-danger', 'Punëtori nuk u largua. Provoni përsëri !');
            return redirect()->back();
        }
    }

    private function reset_ai_article()
    {
        DB::statement('ALTER TABLE `employees` auto_increment = 1;');
    }

    public function getShift($emp)
    {
        $date = date('Y-m-d');
        $employeShift = new EmployeeShiftStart;
        $employeShift = EmployeeShiftStart::where('id_employee', $emp->id_employee)->Where('employee_shift_start_datetime', 'like', '%' . $date . '%')->get();
        return $employeShift;
    }

    public function checkShift($e)
    {
        $date = date('Y-m-d');
        $employeShift = EmployeeShiftStart::Where('id_employee', $e->id_employee)->Where('employee_shift_start_datetime', 'like', '%' . $date . '%')->get();
        return count($employeShift);
    }

    public function createShift(Request $request)
    {
        $new_shift = new  EmployeeShiftStart;
        $user = Auth::user();
        $new_shift->id_employee = $user->id_employee;

        try {
            $new_shift->save();
            return redirect("index");
        } catch (\Exception $ex) {
            $this::index();
        }
    }
}
