<?php

namespace App\Http\Controllers;

use App\Articles;
use App\Purchase;
use App\PurchesDetails;
use Illuminate\Http\Request;

class PurchaseDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id_purchase=$request->purchase_id;
        $id_article=$request->id_article;
        $quantity=$request->quantity;
        $price=$request->price;


        $new_purchaseDetails =  new PurchesDetails;
        $new_purchaseDetails->id_purches=$id_purchase;
        $new_purchaseDetails->id_article=$id_article;
        $new_purchaseDetails->purches_details_quantity=$quantity;
        $new_purchaseDetails->purches_details_price=$price;

        try {
            $new_purchaseDetails->save();
            $article = Articles::where("id_article",$id_article)->get();
            $purchase = Purchase::where('id_purchase',$id_purchase)->get();

            $x = $article[0]->article_quanity;
            $article[0]->article_quanity = ($x+$quantity);
            $article[0]->save();

            $total = $quantity*$price;
            $purchase[0]->purchase_cost_wtvsh += $total;
            $purchase[0]->save();

            return $this->getPurchase($id_purchase);
        }catch(Exception $ex){
            return e;
        }
    }

    public function getPurchase($id_purchase)
    {

        $purchaseDetails = PurchesDetails::where('id_purches', $id_purchase)->get();
        $article  = array();
        $quantity  = array();
        $price  = array();
       // $total  = array();
        foreach ($purchaseDetails as $pD){
            array_push($article,$pD->article->article_name);
            array_push($price,$pD->purches_details_price);
            array_push($quantity,$pD->purches_details_quantity);
        }


        return  response()->json(array("article"=>$article,"price"=>$price,"quantity"=>$quantity));
       // return $purchaseDetails;

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function show(PurchesDetails $purchaseDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchesDetails $purchaseDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchesDetails $purchaseDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseDetails  $purchaseDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchesDetails $purchaseDetails)
    {
        //
    }
}
