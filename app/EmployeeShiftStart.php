<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_employee_shift_start
 * @property int $id_employee
 * @property string $employee_shift_start_datetime
 * @property Employees $employee
 * @property Order[] $orders
 */
class EmployeeShiftStart extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'employee_shift_start';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_employee_shift_start';

    /**
     * @var array
     */
    protected $fillable = ['id_employee', 'employee_shift_start_datetime'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employees', 'id_employee', 'id_employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Orders', 'id_employee_shift_start', 'id_employee_shift_start');
    }
}
