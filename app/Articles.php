<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_article
 * @property int $id_article_unit
 * @property int $id_article_category
 * @property string $article_name
 * @property string $article_serial_number
 * @property float $article_tvsh
 * @property float $article_bought_price
 * @property float $article_selling_price
 * @property string $article_barcode
 * @property int $article_quanity
 * @property int $article_min_quantity
 * @property boolean $article_status
 * @property ArticleCategory $articleCategory
 * @property ArticleUnit $articleUnit
 * @property OrderDetails[] $orderDetails
 */
class Articles extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_article';

    /**
     * @var array
     */
    protected $fillable = ['id_article_unit', 'id_article_category', 'article_name', 'article_serial_number', 'article_tvsh', 'article_bought_price', 'article_selling_price', 'article_barcode', 'article_quanity', 'article_min_quantity', 'article_status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function articleCategory()
    {
        return $this->belongsTo('App\ArticleCategory', 'id_article_category', 'id_article_category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function articleUnit()
    {
        return $this->belongsTo('App\ArticleUnit', 'id_article_unit', 'id_article_unit');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetails', 'id_article', 'id_article');
    }
}
