<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_payment_bank
 * @property string $payment_bank_name
 * @property string $payment_bank_acc_number
 * @property string $payment_bank_swift_code
 * @property boolean $payment_bank_status
 */
class PaymentBank extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'payment_bank';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_payment_bank';

    /**
     * @var array
     */
    protected $fillable = ['payment_bank_name', 'payment_bank_acc_number', 'payment_bank_swift_code', 'payment_bank_status'];

}
