<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_orders_ntables
 * @property int $id_order
 * @property int $id_order_table
 * @property Order $order
 * @property OrderTable $orderTable
 */
class OrderNTables extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'orders_ntables';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_orders_ntables';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id_order', 'id_order_table'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'id_order', 'id_order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderTable()
    {
        return $this->belongsTo('App\OrderTable', 'id_order_table', 'id_order_table');
    }
}
