<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_city
 * @property string $city_name
 * @property Partner[] $partners
 */
class City extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'city';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_city';

    /**
     * @var array
     */
    protected $fillable = ['city_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partners()
    {
        return $this->hasMany('App\Partner', 'id_city', 'id_city');
    }
}
