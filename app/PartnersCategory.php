<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_partner_category
 * @property string $partner_category_description
 * @property Partner[] $partners
 */
class PartnersCategory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'partner_category';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_partner_category';

    /**
     * @var array
     */
    protected $fillable = ['partner_category_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partners()
    {
        return $this->hasMany('App\Partner', 'id_partner_category', 'id_partner_category');
    }
}
