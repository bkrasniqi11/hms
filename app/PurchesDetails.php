<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_purches_details
 * @property int $id_purches
 * @property int $id_article
 * @property string $purches_details_quantity
 * @property string $purches_details_price
 * @property string $updated_at
 * @property string $created_at
 * @property Article $article
 * @property Purchase $purchase
 */
class purchesDetails extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_purches_details';

    /**
     * @var array
     */
    protected $fillable = ['id_purches', 'id_article', 'purches_details_quantity', 'purches_details_price', 'updated_at', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function article()
    {
        return $this->belongsTo('App\Articles', 'id_article', 'id_article');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purchase()
    {
        return $this->belongsTo('App\Purchase', 'id_purches', 'id_purchase');
    }
}
