<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_order_details
 * @property int $id_order
 * @property Articles $article
 * @property int $quantity
 * @property boolean $status
 * @property Order $order
 */
class OrderDetails extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_order_details';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id_order', 'id_article', 'quantity', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Orders', 'id_order', 'id_order');
    }

    public function article()
    {
        return $this->belongsTo('App\Articles', 'id_article', 'id_article');
    }
}
