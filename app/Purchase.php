<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_purchase
 * @property int $id_partner
 * @property string $purchase_invoice_number
 * @property string $purchase_date
 * @property string $purchase_cost_wtvsh
 * @property string $purchase_cost_discount
 * @property string $purchase_cost_tvsh
 * @property Partner $partner
 */
class Purchase extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'purchase';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_purchase';

    /**
     * @var array
     */
    protected $fillable = ['id_partner', 'purchase_invoice_number', 'purchase_date', 'purchase_cost_wtvsh', 'purchase_cost_discount', 'purchase_cost_tvsh'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('App\Partners', 'id_partner', 'id_partner');
    }
}
