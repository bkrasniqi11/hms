<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_order
 * @property int $id_employee_shift_start
 * @property int $id_order_table
 * @property int $id_order_status
 * @property int $id_employee_shift_start_payment
 * @property EmployeeShiftStart $employeeShiftStart
 * @property OrderStatus $orderStatus
 * @property OrderTables  $orderTable
 * @property OrderDetails[] $orderDetails
 * @property OrdersNtable[] $ordersNtables
 */
class Orders extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_order';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['id_employee_shift_start', 'id_order_table', 'id_order_status','id_employee_shift_start_payment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employeeShiftStart()
    {
        return $this->belongsTo('App\EmployeeShiftStart', 'id_employee_shift_start', 'id_employee_shift_start');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderStatus()
    {
        return $this->belongsTo('App\OrderStatus', 'id_order_status', 'id_order_status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderTable()
    {
        return $this->belongsTo('App\OrderTables', 'id_order_table', 'id_order_table');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetails', 'id_order', 'id_order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersNtables()
    {
        return $this->hasMany('App\OrdersNtables', 'id_order', 'id_order');
    }
}
