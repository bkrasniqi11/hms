<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_order_table
 * @property string $order_table_description
 * @property boolean $order_table_status
 * @property boolean $order_table_free
 * @property Order[] $orders
 * @property OrdersNtable[] $ordersNtables
 */
class OrderTables extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_order_table';

    /**
     * @var array
     */
    protected $fillable = ['order_table_description', 'order_table_status', 'order_table_free'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order', 'id_order_table', 'id_order_table');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordersNtables()
    {
        return $this->hasMany('App\OrdersNtable', 'id_order_table', 'id_order_table');
    }

}
