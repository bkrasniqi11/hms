<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_privilege
 * @property string $privileges_description
 * @property Employee[] $employees
 */
class Privileges extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_privilege';

    /**
     * @var array
     */
    protected $fillable = ['privileges_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany('App\Employee', 'id_privilege', 'id_privilege');
    }
}
