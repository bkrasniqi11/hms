<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_payment_type
 * @property string $payment_type_description
 */
class PaymentType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'payment_type';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_payment_type';

    /**
     * @var array
     */
    protected $fillable = ['payment_type_description'];

}
