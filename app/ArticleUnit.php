<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_article_unit
 * @property string $article_unit_description
 * @property Article[] $articles
 */
class ArticleUnit extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'article_unit';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_article_unit';

    /**
     * @var array
     */
    protected $fillable = ['article_unit_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Articles', 'id_article_unit', 'id_article_unit');
    }
}
