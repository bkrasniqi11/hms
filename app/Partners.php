<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_partner
 * @property int $id_partner_category
 * @property int $id_city
 * @property string $partner_name
 * @property string $partner_fiscal_number
 * @property string $partner_bussines_number
 * @property float $partner_tvsh
 * @property string $partner_address
 * @property string $partner_tel
 * @property string $partner_email
 * @property string $partner_web
 * @property string $partner_description
 * @property boolean $partner_status
 * @property City $city
 * @property PartnersCategory $partnersCategory
 * @property Purchase[] $purchases
 */
class Partners extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_partner';

    /**
     * @var array
     */
    protected $fillable = ['id_partner_category', 'id_city', 'partner_name', 'partner_fiscal_number', 'partner_bussines_number', 'partner_tvsh', 'partner_address', 'partner_tel', 'partner_email', 'partner_web', 'partner_description', 'partner_status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'id_city', 'id_city');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partnersCategory()
    {
        return $this->belongsTo('App\PartnersCategory', 'id_partner_category', 'id_partner_category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchases()
    {
        return $this->hasMany('App\Purchase', 'id_partner', 'id_partner');
    }
}
