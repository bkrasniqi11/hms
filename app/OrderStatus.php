<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_order_status
 * @property string $order_status_description
 * @property Orders[] $orders
 */
class OrderStatus extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'order_status';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_order_status';

    /**
     * @var array
     */
    protected $fillable = ['order_status_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Orders', 'id_order_status', 'id_order_status');
    }
}
