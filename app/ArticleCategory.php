<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_article_category
 * @property string $article_category_description
 * @property Article[] $articles
 */
class ArticleCategory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'article_category';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_article_category';

    /**
     * @var array
     */
    protected $fillable = ['article_category_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Article', 'id_article_category', 'id_article_category');
    }
}
