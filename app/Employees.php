<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id_employee
 * @property int $id_privilege
 * @property string $employee_name
 * @property string $employee_surname
 * @property string $employee_tel
 * @property string $username
 * @property string $password
 * @property Privileges $privilege
 * @property EmployeeShiftStart[] $employeeShiftStarts
 */
class Employees extends Authenticatable
{
    use Notifiable;

    protected $guard = 'api';
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_employee';

    /**
     * @var array
     */
    protected $fillable = ['id_privilege', 'employee_name', 'employee_surname', 'employee_tel', 'employee_username', 'employee_password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function privilege()
    {
        return $this->belongsTo('App\Privileges', 'id_privilege', 'id_privilege');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employeeShiftStarts()
    {
        return $this->hasMany('App\EmployeeShiftStart', 'id_employee', 'id_employee');
    }
}
